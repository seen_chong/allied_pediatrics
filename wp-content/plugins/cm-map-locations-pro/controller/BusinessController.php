<?php

namespace com\cminds\maplocations\controller;

use com\cminds\maplocations\model\Business;

use com\cminds\maplocations\helper\GoogleMapsIcons;

use com\cminds\maplocations\model\BusinessCategory;

use com\cminds\maplocations\shortcode\BusinessShortcode;

class BusinessController extends Controller {
	
	const ICON_NONCE_NAME = 'cmloc_business_category_icon_nonce';
	const OFTEN_CRON_SCHEDULE_NAME = 'cmloc_bus_geoloc';
	const RARELY_CRON_SCHEDULE_NAME = 'hourly';
	const CRON_ACTION = 'cmloc_geocode_locations';
	
	const OPTION_BD_INDEX_SHOW_MAP = 'cmloc_bd_index_show_map';
	
	
	protected static $filters = array(
		'cron_schedules',
		'cmdb_index_after_filters' => array('args' => 2),
		'cmbd_options_config',
	);
	protected static $actions = array(
		'init',
		'cm-business-category_edit_form_fields' => array('method' => 'formFields', 'args' => 1),
		'cm-business-category_add_form_fields' => array('method' => 'formFields', 'args' => 1),
		'edited_cm-business-category' => array('method' => 'categoryAfterSave', 'args' => 2),
		'created_cm-business-category' => array('method' => 'categoryAfterSave', 'args' => 2),
		'pre_post_update' => array('args' => 2, 'method' => 'beforePostSave'),
		'save_post' => array('args' => 1, 'method' => 'afterPostSave'),
		self::CRON_ACTION,
	);
	
	static $ajax = array('cmloc_business_map_filter');
	
	
	
	static function init() {
// 		self::cmloc_geocode_locations();
		if (wp_get_schedule(BusinessController::CRON_ACTION) === false) {
			BusinessController::scheduleEvent();
		}
	}
	
	
	static function scheduleEvent($schedule = self::OFTEN_CRON_SCHEDULE_NAME) {
		static::removeScheduledEvent();
		wp_schedule_event(time(), $schedule, BusinessController::CRON_ACTION);
	}
	
	
	static function removeScheduledEvent() {
		wp_clear_scheduled_hook(static::CRON_ACTION);
	}
	
	
	static function cmloc_business_map_filter() {
		echo BusinessShortcode::shortcode($_POST['atts']);
		exit;
	}
	
	

	static function formFields($term = null) {
	
		if (!empty($term) AND is_object($term) AND $category = BusinessCategory::getInstance($term)) {
			$currentIcon = $category->getIcon();
		} else {
			$term = null;
			$currentIcon = null;
		}
	
		$nonceField = static::ICON_NONCE_NAME;
		$nonce = wp_create_nonce(static::ICON_NONCE_NAME);
		$icons = GoogleMapsIcons::getAll();
	
		wp_enqueue_script('cmloc-backend');
	
		echo static::loadBackendView('form-icon', compact('term', 'icons', 'currentIcon', 'nonce', 'nonceField'));
	
	}
	
	
	static function categoryAfterSave($term_id, $term_taxonomy_id = null) {
	
		// Get category object
		$category = BusinessCategory::getInstance($term_id);
		if (empty($category)) return;
	
		if (isset($_POST[static::ICON_NONCE_NAME]) AND wp_verify_nonce($_POST[static::ICON_NONCE_NAME], static::ICON_NONCE_NAME)
			AND !empty($_POST[static::ICON_NONCE_NAME])) {
			$category->setIcon($_POST['cmloc_category_icon']);
		}
	
	}
	
	
	
	static function cmloc_geocode_locations() {
		global $wpdb;
		
		Business::cleanLocationForEmptyAddresses();
		$ids = Business::getIdsNeedGeolocation();
		foreach ($ids as $id) {
			$bus = Business::getInstance($id);
			if ($bus) {
				$bus->setLocationFromAddress();
			}
			Business::clearInstances();
		}
		
		// Reschedule if needed
		$count = count($ids);
		if ($count == 0) { // all remaining posts has been already geolocalized
			if (wp_get_schedule(static::CRON_ACTION) != static::RARELY_CRON_SCHEDULE_NAME) {
				// reschedule the cron to run less frequently
				static::scheduleEvent(static::RARELY_CRON_SCHEDULE_NAME);
			}
		}
		else if ($count > 60) { // more posts remaining
			if (wp_get_schedule(static::CRON_ACTION) != static::OFTEN_CRON_SCHEDULE_NAME) {
				// reschedule cron to run more frequently
				static::scheduleEvent(static::OFTEN_CRON_SCHEDULE_NAME);
			}
		}
		
	}
	
	
	
	static function beforePostSave($postId, $data) {
		if ($bus = Business::getInstance($postId)) {
			if ((isset($_POST['cmbd_address']) AND $bus->getAddress() != $_POST['cmbd_address'])
					OR (isset($_POST['cmbd_cityTown']) AND $bus->getCityTown() != $_POST['cmbd_cityTown'])
					OR (isset($_POST['cmbd_region']) AND $bus->getRegion() != $_POST['cmbd_region'])
					OR (isset($_POST['cmbd_stateCounty']) AND $bus->getstateCounty() != $_POST['cmbd_stateCounty'])
					OR (isset($_POST['cmbd_county']) AND $bus->getCountry() != $_POST['cmbd_county'])
					OR (isset($_POST['cmbd_postalcode']) AND $bus->getPostalCode() != $_POST['cmbd_postalcode'])
			) {
				$bus->setGeocodeUpToDate(false);
			}
		}
	}
	

	static function afterPostSave($postId) {
		if ($bus = Business::getInstance($postId)) {
			$bus->setLocationFromAddress();
		}
	}
	
	
	static function cron_schedules($interval) {
		$interval[static::OFTEN_CRON_SCHEDULE_NAME] = array('interval' => 2 * 60, 'display' => 'Once 2 minutes for CM Locations');
		return $interval;
	}
	
	
	static function cmdb_index_after_filters($content, \WP_Query $query) {
		if (!get_option(static::OPTION_BD_INDEX_SHOW_MAP)) return $content;
		$ids = array();
		foreach ($query->posts as $post) {
			$ids[] = $post->ID;
		}
		$shortcode = BusinessShortcode::shortcode(array(
			'categoryfilter' => 0,
			'searchbar' => 0,
			'ids' => $ids,
		));
		return $content . self::loadFrontendView('index-after-filters', compact('shortcode'));
	}
	
	
	static function cmbd_options_config($options) {
		
		$options[static::OPTION_BD_INDEX_SHOW_MAP] = array(
			'type' => 'bool',
			'default' => 0,
			'category'		 => 'index',
			'subcategory'	 => 'shortcode',
			'title' => 'Show CM Locations Map',
			'desc' => 'Display the CM Map Locations shortcode on the index page.',
		);
		
		return $options;
	}
	
	
}
