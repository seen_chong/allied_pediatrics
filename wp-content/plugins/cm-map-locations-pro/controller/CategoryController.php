<?php

namespace com\cminds\maplocations\controller;

use com\cminds\maplocations\helper\GoogleMapsIcons;

use com\cminds\maplocations\App;

use com\cminds\maplocations\model\Route;

use com\cminds\maplocations\model\Settings;
use com\cminds\maplocations\model\Category;

class CategoryController extends Controller {
	
	const ICON_NONCE_NAME = 'cmloc_category_icon_nonce';
	
	protected static $filters = array('cmloc_category_term_args');
	protected static $actions = array(
		array('name' => 'admin_menu', 'priority' => 11),
		'cmloc_category_edit_form_fields' => array('method' => 'formFields', 'args' => 1),
		'cmloc_category_add_form_fields' => array('method' => 'formFields', 'args' => 1),
		'edited_cmloc_category' => array('method' => 'categoryAfterSave', 'args' => 2),
		'created_cmloc_category' => array('method' => 'categoryAfterSave', 'args' => 2),
		'cmloc_map_filter_after',
		'cmloc_route_editor_middle' => array('args' => 1),
		'cmloc_route_after_save' => array('args' => 1),
	);
	
	static function admin_menu() {
		
		$url = htmlspecialchars(add_query_arg(urlencode_deep(array(
			'taxonomy' => Category::TAXONOMY,
			'post_type' => Route::POST_TYPE
		)), 'edit-tags.php'));
		
		add_submenu_page(App::PREFIX, App::getPluginName() . ' Categories', 'Categories', 'manage_options', $url);
		if( isset($_GET['taxonomy']) && $_GET['taxonomy'] == Category::TAXONOMY && isset($_GET['post_type']) && $_GET['post_type'] == Route::POST_TYPE ) {
			add_filter('parent_file', create_function('$q', 'return "' . App::PREFIX . '";'), 999);
		}
		
	}
	
	
	static function cmloc_category_term_args($args) {
		$args['show_ui'] = true;
		return $args;
	}
	
	
	static function cmloc_map_filter_after() {
		$categories = Category::mapByParent(Category::getAll());
		$currentCategoryId = 0;
		$currentCategorySlug = FrontendController::$query->get(Category::TAXONOMY);
		if ($term = get_term_by('slug', $currentCategorySlug, Category::TAXONOMY)) {
			$currentCategoryId = $term->term_id;
		}
		$baseUrl = FrontendController::getUrl();
		echo self::loadFrontendView('filter', compact('categories', 'currentCategoryId', 'baseUrl'));
	}
	
	
	static function formFields($term = null) {
	
		if (!empty($term) AND is_object($term) AND $category = Category::getInstance($term)) {
			$currentIcon = $category->getIcon();
		} else {
			$term = null;
			$currentIcon = null;
		}
		
		$nonceField = self::ICON_NONCE_NAME;
		$nonce = wp_create_nonce(self::ICON_NONCE_NAME);
		$icons = GoogleMapsIcons::getAll();
		
		wp_enqueue_script('cmloc-backend');
		
		echo self::loadBackendView('form-icon', compact('term', 'icons', 'currentIcon', 'nonce', 'nonceField'));
	
	}
	
	
	static function categoryAfterSave($term_id, $term_taxonomy_id = null) {
		
		// Get category object
		$category = Category::getInstance($term_id);
		if (empty($category)) return;
		
		if (isset($_POST[self::ICON_NONCE_NAME]) AND wp_verify_nonce($_POST[self::ICON_NONCE_NAME], self::ICON_NONCE_NAME)
				AND !empty($_POST[self::ICON_NONCE_NAME])) {
			$category->setIcon($_POST['cmloc_category_icon']);
		}
		
	}
	
	
	
	static function cmloc_route_editor_middle(Route $route) {
		$categoriesTree = Category::getTreeArray(array(), 0, Category::FIELDS_ID_NAME);
		echo self::loadFrontendView('editor', compact('categoriesTree', 'route'));
	}
	
	
	static function cmloc_route_after_save(Route $route) {
		if (!empty($_POST['categories']) AND is_array($_POST['categories'])) {
			$categories = $_POST['categories'];
		} else {
			$categories = array();
		}
		$route->setCategories($categories);
	}
	
	
}
