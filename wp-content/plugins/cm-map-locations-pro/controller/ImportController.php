<?php

namespace com\cminds\maplocations\controller;

use com\cminds\maplocations\model\Location;

use com\cminds\maplocations\model\Route;

use com\cminds\maplocations\helper\KmlHelper;

use com\cminds\maplocations\App;

class ImportController extends Controller {
	
	const NONCE_ACTION_IMPORT = 'cmloc_route_import_nonce';
	const NONCE_ACTION_CSV_IMPORT = 'cmloc_route_import_csv_nonce';
	const NONCE_ACTION_EXPORT = 'cmloc_route_export_nonce';
	const ACTION_EXPORT_ROUTE = 'export-route';
	
	
	protected static $actions = array('admin_menu' => array('priority' => 13));
	protected static $filters = array(
		'post_row_actions' => array('args' => 2),
	);
	
	
	static function admin_menu() {
		add_submenu_page(App::PREFIX, App::getPluginName() . ' Export/Import', 'Export/Import', 'manage_options', self::getMenuSlug(),
			array(get_called_class(), 'render'));
	}
	
	
	static function render() {
		wp_enqueue_style('cmloc-backend');
		wp_enqueue_style('cmloc-settings');
		wp_enqueue_script('cmloc-backend');
		echo self::loadView('backend/template', array(
			'title' => App::getPluginName() . ' Export and Import',
			'nav' => self::getBackendNav(),
			'content' =>
				self::loadBackendView('import', array(
					'formUrl' => admin_url('admin.php?page='. urlencode(self::getMenuSlug())),
					'nonceField' => self::NONCE_ACTION_IMPORT,
					'nonce' => wp_create_nonce(self::NONCE_ACTION_IMPORT),
				)) .
				self::loadBackendView('import-csv', array(
					'formUrl' => admin_url('admin.php?page='. urlencode(self::getMenuSlug())),
					'nonceField' => self::NONCE_ACTION_CSV_IMPORT,
					'nonce' => wp_create_nonce(self::NONCE_ACTION_CSV_IMPORT),
				)) .
				self::loadBackendView('export', array(
					'formUrl' => admin_url('admin.php?page='. urlencode(self::getMenuSlug())),
					'nonceField' => self::NONCE_ACTION_EXPORT,
					'nonce' => wp_create_nonce(self::NONCE_ACTION_EXPORT),
				)),
		));
	}
	
	
	static function getMenuSlug() {
		return App::PREFIX . '-import';
	}
	
	
	static function processRequest() {
		if (!is_admin()) return;
		if (!empty($_POST[self::NONCE_ACTION_IMPORT]) AND wp_verify_nonce($_POST[self::NONCE_ACTION_IMPORT], self::NONCE_ACTION_IMPORT)) {
			static::importFrame();
		}
		else if (!empty($_GET[self::NONCE_ACTION_EXPORT]) AND wp_verify_nonce($_GET[self::NONCE_ACTION_EXPORT], self::NONCE_ACTION_EXPORT)) {
			static::exportSingleKml();
		}
		else if (!empty($_POST[self::NONCE_ACTION_EXPORT]) AND wp_verify_nonce($_POST[self::NONCE_ACTION_EXPORT], self::NONCE_ACTION_EXPORT)) {
			static::exportZipWithKml();	
		}
		else if (!empty($_POST[self::NONCE_ACTION_CSV_IMPORT]) AND wp_verify_nonce($_POST[self::NONCE_ACTION_CSV_IMPORT], self::NONCE_ACTION_CSV_IMPORT)) {
			static::importCsv();
		}
	}
	
	
	
	static protected function importCsv() {
		if (isset($_FILES['csv_file']) AND empty($_FILES['csv_file']['error']) AND is_uploaded_file($_FILES['csv_file']['tmp_name'])) {
			
			$path = $_FILES['csv_file']['tmp_name'];
			
			// Enable this to fix the PHP issue with recognizing line endings:
			ini_set("auto_detect_line_endings", true);
			
			$f = fopen($path, 'r');
			while (($line = fgetcsv($f)) !== false) {
				
				if ($line[1] == 'latitude') continue;
				
				$route = new Route(array(
					'post_title' => $line[0],
					'post_content' => (isset($line[3]) ? $line[3] : ''),
					'post_status' => (isset($line[4]) ? $line[4] : 'draft'),
					'post_date' => current_time('mysql'),
					'post_author' => get_current_user_id(),
					'post_type' => Route::POST_TYPE,
				));
				if ($routeId = $route->save()) {
					
					$location = new Location(array(
						'post_title' => $line[0],
						'post_content' => '',
						'post_status' => 'inherit',
						'post_date' => current_time('mysql'),
						'post_author' => get_current_user_id(),
						'post_type' => Location::POST_TYPE,
						'post_parent' => $routeId,
						'menu_order' => 1,
					));
					if ($locationId = $location->save()) {
						$location->setLat($line[1]);
						$location->setLong($line[2]);
						if (isset($line[5])) $location->setAddress($line[5]);
						if (isset($line[6])) $location->setPostalCode($line[6]);
						if (isset($line[8])) $route->setTags($line[7]);
						if (isset($line[7])) $route->importCategoriesNames($line[8]);
						
						echo '<div>Imported ' . htmlspecialchars($line[0]) .'</div>';
						
					}
					
				} else {
					echo '<div>Error when saving ' . htmlspecialchars($line[0]) .'</div>';
				}
				
			}
			
		}
		echo '<div>END.</div>';
		exit;
	}
	
	
	
	static protected function importFrame() {
		try {
			echo 'Importing...<br />';
			ob_flush();
			flush();
			self::processImportRoutes();
			die('End.');
		} catch (\Exception $e) {
			die('Error: ' . $e->getMessage());
		}
	}
	
	
	
	static protected function exportSingleKml() {
		if (!empty($_GET['id']) AND $route = Route::getInstance($_GET['id'])) {
			header('content-type: application/vnd.google-earth.kml+xml');
			header('Content-Disposition: attachment; filename="'. sanitize_title($route->getTitle()) .'.kml"');
			echo KmlHelper::export($route);
			exit;
		}
	}
	
	
	
	static protected function exportZipWithKml() {
		$upload_dir = wp_upload_dir();
		$zipPath = $upload_dir['path'] .'/'. App::prefix('-export-'. md5(microtime(true))) . '.zip';
			
		$zip = new \ZipArchive();
		if ($res = $zip->open($zipPath, \ZipArchive::CREATE)) {
			$posts = get_posts(array(
				'posts_per_page' => -1,
				'post_type' => Route::POST_TYPE,
				'post_status' => 'any',
			));
			foreach ($posts as $post) {
				if ($route = Route::getInstance($post)) {
					$fileName = sanitize_title($route->getTitle()) .'.kml';
					$zip->addFromString($fileName, KmlHelper::export($route));
					Route::clearInstances();
				}
			}
			$zip->close();
			header('content-type: application/zip');
			header('Content-Disposition: attachment; filename="maps-export-'. current_time('Y-m-d') .'.zip"');
			echo file_get_contents($zipPath);
			unlink($zipPath);
			exit;
		
		}
	}
	
	
	static function processImportRoutes() {
		if (isset($_FILES['cmloc_import_file']) AND empty($_FILES['cmloc_import_file']['error']) AND is_uploaded_file($_FILES['cmloc_import_file']['tmp_name'])) {

			$path = $_FILES['cmloc_import_file']['tmp_name'];
			$fileName = $_FILES['cmloc_import_file']['name'];
			if ('.zip' == substr($fileName, -4, 4)) {
				$zip = new \ZipArchive();
				if ($zip->open($path)) {
					$files = KmlHelper::kmzListFiles($zip);
					foreach ($files as $fileName) {
						$source = KmlHelper::kmzGetSource($zip, $fileName);
						self::processImportSingleRoute($source, $fileName);
					}
				}
				$zip->close();
			} else {
				if ('.kmz' == substr($fileName, -4, 4)) {
					if ($kmlSource = KmlHelper::importReadfile($path, $fileName)) {
						self::processImportSingleRoute($kmlSource, $fileName);
					}
				} else {
					self::processImportSingleRoute(file_get_contents($path), $fileName);
				}
			}
			
		} else {
			throw new \Exception('Invalid upload');
		}
	}
	
	
	static function processImportSingleRoute($source, $fileName) {
		try {
			KmlHelper::importRouteFile(
				$source,
				get_current_user_id(),
				self::getMaxWaypointsParam()
			);
			echo 'Success importing '. $fileName .'<br />';
		} catch (\Exception $e) {
			echo $e->getMessage() . '<br />';
			flush();
			ob_flush();
		}
	}
	
	
	static function getMaxWaypointsParam() {
		if (isset($_POST['max_waypoints']) AND is_numeric($_POST['max_waypoints'])) {
			$maxWaypoints = $_POST['max_waypoints'];
		} else {
			$maxWaypoints = 0;
		}
		if ($maxWaypoints < 1 OR $maxWaypoints > Route::WAYPOINTS_LIMIT) {
			$maxWaypoints = Route::WAYPOINTS_LIMIT;
		}
		return $maxWaypoints;
	}
	

	static function post_row_actions($actions, $post) {
		if ( $post->post_type === Route::POST_TYPE AND $route = Route::getInstance($post) ) {
			$url = add_query_arg(array(
				'page' => ImportController::getMenuSlug(),
				'id' => $route->getId(),
				ImportController::NONCE_ACTION_EXPORT => wp_create_nonce(ImportController::NONCE_ACTION_EXPORT),
			), admin_url('admin.php'));
			$actions['export_kml'] = sprintf('<a href="%s">%s</a>', esc_attr($url), 'Export');
		}
		return $actions;
	}
	
}
