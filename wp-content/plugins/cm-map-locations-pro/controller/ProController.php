<?php

namespace com\cminds\maplocations\controller;

use com\cminds\maplocations\model\Route;

use com\cminds\maplocations\model\Location;

use com\cminds\maplocations\App;
use com\cminds\maplocations\model\Settings;
use com\cminds\maplocations\model\Labels;

class ProController extends Controller {

	protected static $filters = array(
		'cmloc_options_config',
		'cmloc_settings_pages',
		'cmloc_route_icon_url' => array('args' => 2),
	);
	protected static $actions = array(
		array('name' => 'admin_menu', 'priority' => 96),
		array('name' => 'cmloc_labels_init', 'priority' => 10),
		'cmloc_load_assets_frontend',
		'cmloc_single_location_before_images' => array('args' => 1),
		'cmloc_route_editor_middle' => array('args' => 1),
	);
	
	
	static function admin_menu() {
// 		add_submenu_page(App::PREFIX, 'About ' . App::getPluginName(), 'About', 'manage_options', self::getMenuSlug('about'), array(get_called_class(), 'about'));
// 		add_submenu_page(App::PREFIX, App::getPluginName() . ' User Guide', 'User Guide', 'manage_options', self::getMenuSlug('user-guide'),
// 			array(get_called_class(), 'userGuide'));
	}
	
	
	static function getMenuSlug($slug) {
		return App::PREFIX . '-' . $slug;
	}
	

	static function about() {
		echo self::loadView('backend/template', array(
			'title' => 'About ' . App::getPluginName(),
			'nav' => self::getBackendNav(),
			'content' => self::loadBackendView('about', array(
				'iframeURL' => SettingsController::PAGE_ABOUT_URL,
			)) . SettingsController::getSectionExperts(),
		));
	}
	
	
	static function userGuide() {
		echo self::loadView('backend/template', array(
			'title' => App::getPluginName() . ' User Guide',
			'nav' => self::getBackendNav(),
			'content' => self::loadBackendView('about', array(
				'iframeURL' => SettingsController::PAGE_USER_GUIDE_URL,
			)) . SettingsController::getSectionExperts(),
		));
	}
	
	
	
	static function cmloc_labels_init() {
		Labels::loadLabelFile(App::path('asset/labels/pro.tsv'));
	}
	
	
	static function cmloc_options_config($config) {
		return array_merge($config, array(
			Settings::OPTION_INDEX_ORDERBY => array(
				'type' => Settings::TYPE_RADIO,
				'options' => array(
					Settings::ORDERBY_TITLE => 'title',
					Settings::ORDERBY_CREATED => 'created date',
					Settings::ORDERBY_VIEWS => 'views',
				),
				'default' => Settings::DEFAULT_INDEX_ORDERBY,
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Order locations by',
			),
			Settings::OPTION_INDEX_ORDER => array(
				'type' => Settings::TYPE_RADIO,
				'options' => array(
					Settings::ORDER_ASC => 'ascending',
					Settings::ORDER_DESC => 'descending',
				),
				'default' => Settings::DEFAULT_INDEX_ORDER,
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Sorting order',
			),
			Settings::OPTION_UNIT_TEMPERATURE => array(
				'type' => Settings::TYPE_RADIO,
				'options' => array(Settings::UNIT_TEMP_C => 'Celsius', Settings::UNIT_TEMP_F => 'Fahrenheit'),
				'default' => Settings::UNIT_TEMP_C,
				'category' => 'appearance',
				'subcategory' => 'general',
				'title' => 'Temperature units',
				'desc' => 'Used to display the weather.',
			),
			Settings::OPTION_TEMPLATE_OVERRIDE_DIR => array(
				'type' => Settings::TYPE_STRING,
				'category' => 'appearance',
				'subcategory' => 'general',
				'title' => 'Override templates with files from a directory',
				'desc' => 'Enter the directory path relative to the wp-content directory to override the plugin\'s view templates.',
			),
			Settings::OPTION_OPENWEATHERMAP_API_KEY => array(
				'type' => Settings::TYPE_STRING,
				'category' => 'general',
				'subcategory' => 'api',
				'title' => 'OpenWeatherMap.org API Key',
				'desc' => 'Enter the OpenWeatherMap.org API key.<br /><a target="_blank" '. 'href="http://openweathermap.org/appid">Get the API key from here</a>.',
			),
			Settings::OPTION_INDEX_LOCATIONS_LIST_LAYOUT => array(
				'type' => Settings::TYPE_RADIO,
				'default' => Settings::INDEX_LIST_BOTTOM,
				'options' => array(
					Settings::INDEX_LIST_BOTTOM => 'Bottom',
					Settings::INDEX_LIST_BOTTOM_CONDENSED => 'Bottom condensed',
					Settings::INDEX_LIST_LEFT => 'Left',
					Settings::INDEX_LIST_RIGHT => 'Right',
				),
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Where to show locations list',
				'desc' => 'Choose where to display the locations list on the index page.',
			),
			
			// Access Control
			Settings::OPTION_ACCESS_MAP_INDEX => array(
				'type' => Settings::TYPE_SELECT,
				'options' => array(App::namespaced('model\Settings'), 'getAccessOptions'),
				'default' => Settings::ACCESS_GUEST,
				'category' => 'access',
				'subcategory' => 'access',
				'title' => 'List locations',
				'desc' => 'Select who can access the locations index and also search or filter locations.',
			),
			Settings::OPTION_ACCESS_MAP_INDEX_CAP => array(
				'type' => Settings::TYPE_STRING,
				'category' => 'access',
				'subcategory' => 'access',
				'default' => '',
				'title' => 'List locations custom capability',
				'desc' => 'Enter a capability name which will be required for user to show locations index, search or filter locations.<br />'
				. 'Read about <a href="https://codex.wordpress.org/Roles_and_Capabilities">Roles and Capabilities</a> on Wordpress Codex.',
			),
			Settings::OPTION_ACCESS_MAP_VIEW => array(
				'type' => Settings::TYPE_SELECT,
				'options' => array(App::namespaced('model\Settings'), 'getAccessOptions'),
				'default' => Settings::ACCESS_GUEST,
				'category' => 'access',
				'subcategory' => 'access',
				'title' => 'View location',
				'desc' => 'Select who can display the location\'s page.',
			),
			Settings::OPTION_ACCESS_MAP_VIEW_CAP => array(
				'type' => Settings::TYPE_STRING,
				'category' => 'access',
				'subcategory' => 'access',
				'default' => '',
				'title' => 'View map custom capability',
				'desc' => 'Enter a capability name which will be required for user to display a map page.<br />'
					. 'Read about <a href="https://codex.wordpress.org/Roles_and_Capabilities">Roles and Capabilities</a> on Wordpress Codex.',
			),
			Settings::OPTION_ACCESS_MAP_CREATE => array(
				'type' => Settings::TYPE_SELECT,
				'options' => array(App::namespaced('model\Settings'), 'getAccessOptionsWithoutGuest'),
				'default' => Settings::ACCESS_USER,
				'category' => 'access',
				'subcategory' => 'access',
				'title' => 'Create and update location',
				'desc' => 'Select who can create locations.',
			),
			Settings::OPTION_ACCESS_MAP_CREATE_CAP => array(
				'type' => Settings::TYPE_STRING,
				'category' => 'access',
				'subcategory' => 'access',
				'default' => 'read',
				'title' => 'Create and update location capability',
				'desc' => 'Enter a capability name which will be required for user to create or update own location.<br />'
					. 'Read about <a href="https://codex.wordpress.org/Roles_and_Capabilities">Roles and Capabilities</a> on Wordpress Codex.',
			),
			Settings::OPTION_INDEX_MAP_MARKER_CLICK => array(
				'type' => Settings::TYPE_RADIO,
				'default' => Settings::DEFAULT_INDEX_MAP_MARKER_CLICK,
				'options' => array(
					Settings::ACTION_CLICK_REDIRECT => 'Open the location\'s page',
					Settings::ACTION_CLICK_TOOLTIP => 'Show tooltip with information about the location',
				),
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Click on the map marker will',
			),
			Settings::OPTION_INDEX_LIST_ITEM_CLICK => array(
				'type' => Settings::TYPE_RADIO,
				'default' => Settings::DEFAULT_INDEX_LIST_ITEM_CLICK,
				'options' => array(
					Settings::ACTION_CLICK_REDIRECT => 'Open the location\'s page',
					Settings::ACTION_CLICK_TOOLTIP => 'Show tooltip with information about the location',
				),
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Click on the location\'s list item will',
			),
			Settings::OPTION_TOOLTIP_DESCRIPTION_CHARS => array(
				'type' => Settings::TYPE_INT,
				'default' => Settings::DEFAULT_TOOLTIP_DESCRIPTION_CHARS,
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Number of characters from the location\'s description to display on the tooltip',
			),
			
			Settings::OPTION_INDEX_ZIP_RADIUS_FILTER_ENABLE => array(
				'type' => Settings::TYPE_BOOL,
				'default' => 0,
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Enable ZIP code radius filter',
				'desc' => 'If enabled the ZIP code radius filter will be added to the index page next to the search box.',
			),
			Settings::OPTION_INDEX_ZIP_RADIUS_COUNTRY => array(
				'type' => Settings::TYPE_STRING,
				'default' => 'USA',
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Country code for the ZIP code searching',
				'desc' => 'The ZIP filter will work only within a single country.',
			),
			Settings::OPTION_INDEX_ZIP_RADIUS_MIN => array(
				'type' => Settings::TYPE_INT,
				'default' => 10,
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Minimum radius value',
			),
			Settings::OPTION_INDEX_ZIP_RADIUS_MAX => array(
				'type' => Settings::TYPE_INT,
				'default' => 1000,
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Maximum radius value',
			),
			Settings::OPTION_INDEX_ZIP_RADIUS_STEP => array(
				'type' => Settings::TYPE_INT,
				'default' => 10,
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Radius value step',
			),
			Settings::OPTION_INDEX_ZIP_RADIUS_DEFAULT => array(
				'type' => Settings::TYPE_INT,
				'default' => 10,
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Radius default value',
			),
			Settings::OPTION_INDEX_ZIP_RADIUS_GEOLOCATION => array(
				'type' => Settings::TYPE_BOOL,
				'default' => 0,
				'category' => 'appearance',
				'subcategory' => 'zip',
				'title' => 'Enable geolocation',
				'desc' => 'If enabled the user\'s ZIP code will be recognized using browser\'s geolocation API.'
						.'<br />Notice that the geolocation API works only if you\'re using https.',
			),
			
			Settings::OPTION_MAP_DEFAULT_ICON_URL => array(
				'type' => Settings::TYPE_STRING,
				'default' => '',
				'category' => 'appearance',
				'subcategory' => 'index',
				'title' => 'Default location\'s icon URL',
				'desc' => 'If there\'s not location\'s icon this will be used instead.',
			),
			
		));
	}
	
	
	static function cmloc_load_assets_frontend() {
		wp_enqueue_script('cmloc-froogaloop', App::url('asset/js/froogaloop/froogaloop-min.js'), null, App::VERSION);
	}
	
	
	static function cmloc_route_single_params_names($params) {
		$params['weather'] = 'Weather';
		return $params;
	}
	
	
	static function cmloc_route_index_params_names($params) {
// 		$params['featured_image'] = 'Featured image';
		return $params;
	}
	
	
	static function cmloc_single_location_before_images(Location $location) {
		printf('<a target="_blank" class="cmloc-weather" title="%s"></a>', esc_attr(Labels::getLocalized('check_weather')));
	}
	
	
	static function cmloc_settings_pages($categories) {
		$categories['access'] = 'Access Control';
		$categories['labels'] = 'Labels';
		return $categories;
	}
	
	
	static function cmloc_route_editor_middle(Route $route) {
		echo DashboardController::loadFrontendView('editor-icon', compact('route'));
	}
	
	
	static function cmloc_route_icon_url($iconUrl, $route) {
		if ($defaultIcon = Settings::getOption(Settings::OPTION_MAP_DEFAULT_ICON_URL) AND empty($iconUrl)) {
			$iconUrl = $defaultIcon;
		}
		return $iconUrl;
	}
	

}
