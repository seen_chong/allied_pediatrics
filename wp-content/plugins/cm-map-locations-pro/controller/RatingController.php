<?php

namespace com\cminds\maplocations\controller;

use com\cminds\maplocations\helper\RouteView;

use com\cminds\maplocations\model\Route;

class RatingController extends Controller {
	
	const NONCE_RATING_ACTION = 'route_rating';
	
	static $actions = array(
		'cmloc_load_single_page_scripts',
		'cmloc_route_map_before_top' => array('args' => 1),
	);
	static $ajax = array('cmloc_route_rating');
	
	
	static function cmloc_load_single_page_scripts() {
		wp_enqueue_script('cmloc-location-rating');
		wp_localize_script('cmloc-location-rating', 'CMLOC_Rating', array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce(self::NONCE_RATING_ACTION),
		));
	}
	
	
	static function cmloc_route_rating() {
		$response = array('success' => 0);
		if (!empty($_POST['nonce']) AND wp_verify_nonce($_POST['nonce'], self::NONCE_RATING_ACTION)) {
			if (!empty($_POST['routeId']) AND !empty($_POST['rate'])) {
				if ($route = Route::getInstance($_POST['routeId'])) {
					if ($route->canRate()) {
						if (!$route->didUserRate()) {
							if ($route->rate($_POST['rate'])) {
								$response['success'] = 1;
								$response['rate'] = $route->getRate();
							} else {
								$response['msg'] = 'Cannot rate route.';
							}
						} else {
							$response['msg'] = 'User already did rate this route.';
						}
					} else {
						$response['msg'] = 'User is not allowed to rate.';
					}
				} else {
					$response['msg'] = 'Route not found.';
				}
			} else {
				$response['msg'] = 'Invalid request.';
			}
		} else {
			$response['msg'] = 'Access denied.';
		}
		header('content-type: application/json');
		echo json_encode($response);
		exit;
	}
	
	
	static function cmloc_route_map_before_top(Route $route) {
		printf('<div class="cmloc-rating-outer">%s</div>', RouteView::displayRating($route));
	}
	
	
}