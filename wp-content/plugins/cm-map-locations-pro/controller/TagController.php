<?php

namespace com\cminds\maplocations\controller;

use com\cminds\maplocations\model\Route;

class TagController extends Controller {
	
	protected static $actions = array(
		'cmloc_route_editor_middle' => array('args' => 1),
		'cmloc_route_after_save' => array('args' => 1),
	);
	
	
	static function cmloc_route_editor_middle(Route $route) {
		echo self::loadFrontendView('editor', compact('route'));
	}
	
	
	static function cmloc_route_after_save(Route $route) {
		if (!empty($_POST['tags'])) {
			$tags = $_POST['tags'];
		} else {
			$tags = array();
		}
		$route->setTags($tags);
	}
	
	
}