<?php

namespace com\cminds\maplocations\helper;

use com\cminds\maplocations\model\Attachment;

use com\cminds\maplocations\model\Category;

use com\cminds\maplocations\App;

use com\cminds\maplocations\model\Route;
use com\cminds\maplocations\model\Location;

class KmlHelper {
	
	const UPLOAD_DIR = 'cmloc';
	const DEBUG_MODE = 1;
	
	static function export(Route $route) {
		
		$locations = $route->getLocations();
		
		/* @var $firstLocation Location */
		$firstLocation = reset($locations);
		/* @var $lastLocation Location */
		$lastLocation = end($locations);
		
		$waypoints = array_map(function($val) {
			return $val[1].','.$val[0];
		}, KmlHelper::decodePolylineToArray($route->getOverviewPath()));
		
		$placemarks = array();
		foreach ($locations as $i => $location) {
			/* @var $location Location */
			$placemarks[] = self::getPlacemark($location);
		}
		
		$tempalte = file_get_contents(App::path('asset/kml-template.xml'));
		return strtr($tempalte, array(
			'{{routeName}}' => $route->getTitle(),
			'{{routeDescription}}' => $route->getContent(),
			'{{distance}}' => $route->getDistance(),
			'{{duration}}' => $route->getDuration(),
			'{{speed}}' => $route->getAvgSpeed(),
			'{{minAltitude}}' => $route->getMinElevation(),
			'{{maxAltitude}}' => $route->getMaxElevation(),
			'{{climb}}' => $route->getElevationGain(),
			'{{descent}}' => $route->getElevationDescent(),
			'{{routeExtendedData}}' => self::getRouteExtendedData($route),
			'{{placemarks}}' => implode(PHP_EOL, $placemarks),
			'{{pathCoordinates}}' => implode(PHP_EOL, $waypoints),
		));
		
	}
	
	
	static function importSingleRoute($path, $fileName, $authorId, $maxWaypoints) {
		set_time_limit(3600);
		if ($kmlSource = self::importReadfile($path, $fileName)) {
			return self::importRouteFile($kmlSource, $authorId, $maxWaypoints);
		} else {
			throw new \Exception('Failed to load KML file.');
		}
	}
	
	
	
	static function importRouteFile($kmlSource, $authorId, $maxWaypoints) {
		set_time_limit(3600);
		
		$kmlSource = str_replace('xmlns=', 'ns=', $kmlSource);
		
		/* @var $xml SimpleXMLElement */
		$xml = simplexml_load_string($kmlSource);
		
		$routeData = self::getRouteData($xml);
		$route = self::importCreateRoute($xml, $routeData, $authorId);
		$routeId = $route->getId();
		
		if (!empty($routeData['permalink'])) {
			self::importLocations($route, $xml, $routeData, $maxWaypoints);
		} else {
			self::importOptimizeLocations($route, $xml, $routeData, $maxWaypoints);
		}
			
		return $route;
		
	}
	
	
	static function importLocations(Route $route, $xml, $routeData, $maxWaypoints) {
		
		set_time_limit(3600);
		$routeId = $route->getId();
		$authorId = $route->getAuthorId();
		
		$placemarks = self::getDocPlacemarks($xml);
		
		foreach ($placemarks as $i => $placemark) {
			$title = (string)$placemark['node']->name;
			if ($placemark['node']->styleUrl AND (string)$placemark['node']->styleUrl == '#cmRouteLocationWaypointStyle') {
				$type = Location::TYPE_WAYPOINT;
			} else {
				$type = Location::TYPE_LOCATION;
			}
			try {
				$location = self::importCreateLocation($authorId, $routeId, $title, $type,
					$placemark['data']['latitude'], $placemark['data']['longitude'], $i, $placemark);
			} catch (\Exception $e) {
				self::debug($e->getMessage());
			}
			Location::clearInstances();
		}
		
// 		if (!$route->getOverviewPath()) {
// 			$route->recalculateOverviewPath();
// 		}
		
	}
	
	
	
	static function importOptimizeLocations(Route $route, $xml, $routeData, $maxWaypoints) {
		
		set_time_limit(3600);
		$routeId = $route->getId();
		$authorId = $route->getAuthorId();
		
		$placemarks = self::getDocPlacemarks($xml);
		$path = self::getDocPath($xml);
		$map = self::createPlacemarksToWaypointsMap($placemarks, $path);
		$nextPlacemark = 0;
		
// 		$lines = $xml->xpath('//LineString');
		
		$count = count($path);
		if ($route->getTravelMode() != 'DIRECT') {
			$maxWaypoints = count($placemarks);
		}
		$step = max(1, $count/($maxWaypoints-1));
		$last = null;
		$counter = 0;
		$placemarkCount = count($placemarks);
		$placemarkMapCount = count($map);
		
		self::debug("placemarkCount = $placemarkCount");
		self::debug("placemarkMapCount = $placemarkMapCount");
		self::debug("count = $count");
		self::debug("step = $step");
		self::debug("maxwaypoints = $maxWaypoints");
		
		for ($i=0; $i<$count; $i+=$step) {
			
			self::debug('---------------------------------');
			self::debug("i = $i");
			self::debug("step = $step");
			if (isset($map[$nextPlacemark])) {
				self::debug("nextPlacemark = $nextPlacemark at ". $map[$nextPlacemark]);
			}
			
			if ($i+$step >= $count AND $i != $count-1 AND $step != 1) {
				self::debug("change step from $step to ". ($count-$i-1));
				$step = $count-$i-1;
			}
			else if (!empty($map) AND !empty($map[$nextPlacemark]) AND $i+$step >= $map[$nextPlacemark]) {
				self::debug('change i - go to placemark '. $nextPlacemark .' at '. $map[$nextPlacemark]);
				$i = $map[$nextPlacemark];
				self::debug("new i = $i");
			}
			
			$key = ceil($i);
			if ($key >= $count) break;
			
			self::debug("key = $key");

			$coord = $path[$key];
// 			self::debug($coord);

			$placemark = null;
			if (!empty($map) AND $key == $map[$nextPlacemark]) {
				$title = (string)$placemarks[$nextPlacemark]['node']->name;
				$placemark = $placemarks[$nextPlacemark];
// 				self::debug($placemark['node']->styleUrl);
				if ($placemark['node']->styleUrl AND (string)$placemark['node']->styleUrl == '#cmRouteLocationWaypointStyle') {
					$type = Location::TYPE_WAYPOINT;
				} else {
					$type = Location::TYPE_LOCATION;
				}
				$counter++;
			}
			else if ($key == 0) {
				$title = 'Start';
				$type = Location::TYPE_LOCATION;
				$counter++;
			}
			else if ($key == $count-1) {
				$title = 'End';
				$type = Location::TYPE_LOCATION;
				$counter++;
			}
			else {
				$title = 'Waypoint';
				$type = Location::TYPE_WAYPOINT;
			}
			
			try {
				$location = self::importCreateLocation($authorId, $routeId, $title, $type, $coord[1], $coord[0], $key, $placemark);
// 				self::debug($location);
			} catch (\Exception $e) {
				self::debug($e->getMessage());
			}
			Location::clearInstances();
			if (isset($map[$nextPlacemark]) AND $key == $map[$nextPlacemark]) {
				$nextPlacemark++;
			}

			self::debugFlush();

		}
		
// 		if (!$route->getOverviewPath()) {
// 			$route->recalculateOverviewPath();
// 		}
		
	}
	
	
	static function debug($msg) {
		if (self::DEBUG_MODE) {
			var_dump($msg);
		}
	}
	
	
	static function debugFlush() {
		if (self::DEBUG_MODE) {
			ob_flush();
			flush();
		}
	}
	
	
	static protected function getDocPlacemarks(\SimpleXMLElement $xml) {
		$placemarks = $xml->xpath('//Placemark/styleUrl[contains(., "cmLocationStyle")]/..');
// 		self::debug($placemarks);exit;
		foreach ($placemarks as &$placemark) {
			$placemark = array(
				'node' => $placemark,
				'data' => self::getPlacemarkData($placemark),
				'coords' => explode(',', $placemark->Point->coordinates),
			);
		}
		return $placemarks;
	}
	
	
	static protected function getDocPath(\SimpleXMLElement $xml) {
		$lines = $xml->xpath('//LineString');
		foreach ($lines as $line) {
			if ($line->coordinates) {
				return array_map(function($row) {
					return explode(',', $row);
				}, array_filter(preg_split('/\s/', (string)$line->coordinates)));
			}
			break;
		}
	}
	
	
	static protected function createPlacemarksToWaypointsMap($placemarks, $path) {
		
		$pathCount = count($path);
		$placemarksCount = count($placemarks);
		
		$placemarksMap = array();
		foreach ($placemarks as $i => $placemark) {
			$bestWaypoint = null;
			if ($i == 0) {
				$bestWaypoint = 0;
			}
			else if ($i == $placemarksCount) {
				$bestWaypoint = count($path)-1;
			} else {
				$bestWaypointDist = PHP_INT_MAX;
				foreach ($path as $w => $waypoint) {
					$dist = self::calculateDistance($placemark['coords'][1], $placemark['coords'][0], $waypoint[1], $waypoint[0]);
					if ($dist < $bestWaypointDist) {
						$bestWaypointDist = $dist;
						$bestWaypoint = $w;
					}
				}
			}
			$placemarksMap[$i] = $bestWaypoint;
		}
		return $placemarksMap;
		
	}
	
	
	static function calculateDistance($p1Lat, $p1Long, $p2Lat, $p2Long) {
	
		$R = 6371000; // metres
		$k = deg2rad($p1Lat);
		$l = deg2rad($p2Lat);
		$m = deg2rad($p2Lat - $p1Lat);
		$n = deg2rad($p2Long - $p1Long);
	
		$a = sin($m/2) * sin($m/2) +
				cos($k) * cos($l) *
				sin($n/2) * sin($n/2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
	
		return $R * $c;
	
	}
	
	
	static protected function importCreateRoute($xml, array $routeData, $authorId) {
		
		$route = new Route();
		$route->setTitle($xml->Document->name);
		$route->setContent($xml->Document->description);
		$route->setStatus(empty($routeData['status']) ? 'draft' : $routeData['status']);
		$route->setAuthor($authorId);
		if (!empty($data['created'])) $route->setCreated($data['created']);
		if (!$route->save()) {
			throw new \Exception('Failed to save route.');
		}
			
		$route->setTravelMode(!empty($routeData['travelMode']) ? $routeData['travelMode'] : 'DIRECT');
		if (!empty($routeData['distance'])) {
			$route->setDistance($routeData['distance']);
		}
		if (!empty($routeData['duration'])) {
			$route->setDuration($routeData['duration']);
		}
		if (!empty($routeData['avgSpeed'])) {
			$route->setAvgSpeed($routeData['avgSpeed']);
		}
		if (!empty($routeData['maxAltitude'])) {
			$route->setMaxElevation($routeData['maxAltitude']);
		}
		if (!empty($routeData['minAltitude'])) {
			$route->setMinElevation($routeData['minAltitude']);
		}
		if (!empty($routeData['climb'])) {
			$route->setElevationGain($routeData['climb']);
		}
		if (!empty($routeData['descent'])) {
			$route->setElevationDescent($routeData['descent']);
		}
		if (!empty($routeData['categories'])) {
			$route->setCategoriesByNames(explode(',', $routeData['categories']));
		}
		if (!empty($routeData['tags'])) {
			$route->setTags($routeData['tags']);
		}
		if (!empty($routeData['overviewPath'])) {
			$route->setOverviewPath($routeData['overviewPath']);
		}
		if (!empty($routeData['pathColor'])) {
			$route->setPathColor($routeData['pathColor']);
		}
		if (!empty($routeData['useMinorLengthUnits'])) {
			$route->setMinorLengthUnits($routeData['useMinorLengthUnits']);
		}
		if (!empty($routeData['showWeatherPerLocation'])) {
			$route->setWeatherPerLocation($routeData['showWeatherPerLocation']);
		}
		if (!empty($routeData['images'])) {
			self::importImages($route->getId(), $routeData['images']);
		}
		if (!empty($routeData['icon'])) {
			$route->setIcon($routeData['icon']);
		}
		
		return $route;
	}
	
	
	static function importImages($postId, $images) {
		if (!is_array($images)) $images = explode(PHP_EOL, $images);
		$images = array_filter(array_map('trim', $images));
		foreach ($images as $imageUrl) {
			$fileName = explode('/', $imageUrl);
			$fileName = end($fileName);
			$fileTargetPath = self::getUploadDir(self::UPLOAD_DIR) . floor(microtime(true)*1000) . $fileName;
			$result = @file_put_contents($fileTargetPath, file_get_contents($imageUrl));
			$ext = substr($fileName, strrpos($fileName, '.')+1, 5);
			$mimeType = 'image/'. $ext;
			if ($result) {
				$attachment = array(
					'guid'           => $fileTargetPath,
					'post_mime_type' => $mimeType,
					'post_title'     => sanitize_title($fileName),
					'post_content'   => '',
					'post_status'    => 'inherit',
					'post_type'		 => Attachment::POST_TYPE,
				);
				$attach_id = wp_insert_attachment($attachment, $fileTargetPath, $postId);
				require_once(ABSPATH . 'wp-admin/includes/image.php');
				require_once(ABSPATH . 'wp-admin/includes/media.php');
				$attach_data = wp_generate_attachment_metadata($attach_id, $fileTargetPath);
				wp_update_attachment_metadata($attach_id, $attach_data);
			}
		}
	}
	
	
	public static function getUploadDir($name) {
		$uploadDir = wp_upload_dir();
		if ($uploadDir['error']) {
			throw new Exception(__('Error while getting wp_upload_dir():' . $uploadDir['error']));
		} else {
			$dir = $uploadDir['basedir'] . '/' . static::UPLOAD_DIR . '/' . $name . '/';
			if(!is_dir($dir)) {
				if(!wp_mkdir_p($dir)) {
					throw new Exception(__('Script couldn\'t create the upload folder:' . $dir));
				}
			}
			return $dir;
		}
	}
	
	
	
	static function getRouteData(\SimpleXMLElement $xml) {
		if ($xml->Document AND $xml->Document->ExtendedData AND $xml->Document->ExtendedData->Data) {
			return self::getExtendedData($xml->Document->ExtendedData->Data);
		} else {
			return array();
		}
	}
	
	
	static function getPlacemarkData(\SimpleXMLElement $placemark) {
		if ($placemark->ExtendedData AND $placemark->ExtendedData->Data) {
			return self::getExtendedData($placemark->ExtendedData->Data);
		} else {
			return array();
		}
	}
	
	
	static function getExtendedData(\SimpleXMLElement $xml) {
		$data = array();
		foreach ($xml as $node) {
			$data[(string)$node['name']] = (string)$node;
		}
		return $data;
	}
	
	
	static protected function importCreateLocation($authorId, $routeId, $title, $type, $lat, $long, $menuOrder, $placemark) {
		$location = new Location(array(
			'post_parent' => $routeId,
			'post_author' => $authorId,
			'post_type' => Location::POST_TYPE,
			'post_status' => 'inherit',
			'ping_status' => 'closed',
			'comment_status' => 'closed',
		));
		$location->setTitle($title);
		$location->setMenuOrder($menuOrder);
		if ($placemark AND $placemark['node']->description) {
			$location->setContent((string)$placemark['node']->description);
		}
		if ($locationId = $location->save()) {
			$location->setLat($lat);
			$location->setLong($long);
			$location->setLocationType($type);
			
			if ($placemark AND !empty($placemark['data'])) {
				if (!empty($placemark['data']['address'])) $location->setAddress($placemark['data']['address']);
				if (!empty($placemark['data']['latitude'])) $location->setLat($placemark['data']['latitude']);
				if (!empty($placemark['data']['longitude'])) $location->setLong($placemark['data']['longitude']);
				if (!empty($placemark['data']['altitude'])) $location->setAltitude($placemark['data']['altitude']);
			}
			
		} else {
			return false;
		}
		return $location;
	}
	
	
	
	
	static function importReadfile($path, $fileName) {
		$content = '';
		if ('.kmz' == substr($fileName, -4, 4)) {
			$zip = new \ZipArchive();
			if ($zip->open($path)) {
				
				$fileName = null;
				for ($i=0; $i<$zip->numFiles; $i++) {
					$name = $zip->getNameIndex($i);
					if ('.kml' == substr($name, -4, 4)) {
						$fileName = $name;
						break;
					}
				}
				if ($fileName AND $fp = $zip->getStream($fileName)) {
					while (!feof($fp)) {
						$content .= fread($fp, 2);
					}
					fclose($fp);
				}
				
			}
			$zip->close();
			
		} else {
			$content = file_get_contents($path);
		}
		
		if (!empty($content)) {
			return $content;
		}
		
	}
	
	
	static protected function getPlacemark(Location $location) {
		
		$imagesUrls = array_map(function(Attachment $image) {
			if ($image->isImage()) {
				return $image->getImageUrl(Attachment::IMAGE_SIZE_FULL);
			} else {
				return $image->getUrl();
			}
		}, $location->getImages());
		
		return '<Placemark>
			<styleUrl>#cmLocationStyle</styleUrl>
			<name>'. htmlspecialchars($location->getTitle()) .'</name>
			<description><![CDATA['. $location->getContent() .']]></description>
			<ExtendedData>
				<Data name="address">'. htmlspecialchars($location->getAddress()) .'</Data>
				<Data name="created">'. htmlspecialchars($location->getCreatedDate()) .'</Data>
				<Data name="modified">'. htmlspecialchars($location->getModifiedDate()) .'</Data>
				<Data name="authorId">'. htmlspecialchars($location->getAuthorId()) .'</Data>
				<Data name="authorEmail">'. htmlspecialchars($location->getAuthorEmail()) .'</Data>
				<Data name="authorName">'. htmlspecialchars($location->getAuthorDisplayName()) .'</Data>
				<Data name="locationType">'. htmlspecialchars($location->getLocationType()) .'</Data>
				<Data name="status">'. htmlspecialchars($location->getStatus()) .'</Data>
				<Data name="menuOrder">'. htmlspecialchars($location->getMenuOrder()) .'</Data>
				<Data name="longitude">'. htmlspecialchars($location->getLong()) .'</Data>
				<Data name="latitude">'. htmlspecialchars($location->getLat()) .'</Data>
				<Data name="altitude">'. htmlspecialchars($location->getAltitude()) .'</Data>
				<Data name="images"><![CDATA['. implode(PHP_EOL, $imagesUrls) .']]></Data>
			</ExtendedData>
			<Point>
				<coordinates>'. $location->getLong() .','. $location->getLat() .','. $location->getAltitude() .'</coordinates>
			</Point>
		</Placemark>';
	}
	
	
	static protected function getRouteExtendedData(Route $route) {
		$out = '';
		$data = array(
// 			'distance' => $route->getDistance(),
// 			'duration' => $route->getDuration(),
// 			'avgSpeed' => $route->getAvgSpeed(),
// 			'minAltitude' => $route->getMinElevation(),
// 			'maxAltitude' => $route->getMaxElevation(),
// 			'climb' => $route->getElevationGain(),
// 			'descent' => $route->getElevationDescent(),
// 			'categories' => implode(',', $route->getCategories(Category::FIELDS_NAMES)),
// 			'tags' => implode(',', $route->getTags(Category::FIELDS_NAMES)),
// 			'created' => $route->getCreatedDate(),
// 			'modified' => $route->getModifiedDate(),
// 			'overviewPath' => $route->getOverviewPath(),
// 			'pathColor' => $route->getPathColor(),
			'permalink' => $route->getPermalink(),
			'status' => $route->getStatus(),
			'icon' => $route->getIconUrl(),
// 			'travelMode' => $route->getTravelMode(),
			'slug' => $route->getSLug(),
// 			'useMinorLengthUnits' => $route->useMinorLengthUnits() ? 1 : 0,
// 			'showWeatherPerLocation' => $route->showWeatherPerLocation() ? 1 : 0,
		);
		foreach ($data as $key => $val) {
			$out .= '<Data name="'. htmlspecialchars($key) .'">'. htmlspecialchars($val) .'</Data>' . PHP_EOL;
		}
		
		$imagesUrls = array_map(function(Attachment $image) {
			return $image->getImageUrl(Attachment::IMAGE_SIZE_FULL);
		}, $route->getImages());
		$out .= '<Data name="images"><![CDATA['. implode(PHP_EOL, $imagesUrls) .']]></Data>';
		
		return $out;
		
	}
	

	static function decodePolylineToArray($encoded) {
		$length = strlen($encoded);
		$index = 0;
		$points = array();
		$lat = 0;
		$lng = 0;

		while ($index < $length) {
			$b = 0;
			$shift = 0;
			$result = 0;
			do {
				$b = ord(substr($encoded, $index++)) - 63;
				$result |= ($b & 0x1f) << $shift;
				$shift += 5;
			} while ($b >= 0x20);
			$dlat = (($result & 1) ? ~($result >> 1) : ($result >> 1));
			$lat += $dlat;
			$shift = 0;
			$result = 0;
			do {
				$b = ord(substr($encoded, $index++)) - 63;
				$result |= ($b & 0x1f) << $shift;
				$shift += 5;
			} while ($b >= 0x20);
			$dlng = (($result & 1) ? ~($result >> 1) : ($result >> 1));
			$lng += $dlng;
			$points[] = array($lat * 1e-5, $lng * 1e-5);
		}
		return $points;
	}
	
	
	static function kmzListFiles(\ZipArchive $zip, $ext = 'kml') {
		$files = array();
		$c = strlen($ext)+1;
		for ($i=0; $i<$zip->numFiles; $i++) {
			$name = $zip->getNameIndex($i);
			if (is_null($ext) OR '.'. $ext == substr($name, -$c, $c)) {
				$files[] = $name;
			}
		}
		return $files;
	}
	
	
	static function kmzGetSource(\ZipArchive $zip, $fileName) {
		$content = '';
		if ($fp = $zip->getStream($fileName)) {
			while (!feof($fp)) {
				$content .= fread($fp, 2);
			}
			fclose($fp);
		}
		return $content;
	}
	

}
