<?php

namespace com\cminds\maplocations\model;

use com\cminds\maplocations\controller\RouteController;
use com\cminds\maplocations\helper\BusinessLocationsQuery;

class Business extends PostType {
	
	const POST_TYPE = 'cm-business';
	
	const META_ADDRESS = 'cmbd_address';
	const META_CITY_TOWN = 'cmbd_cityTown';
	const META_POSTAL_CODE = 'cmbd_postalcode';
	const META_COUNTRY = 'cmbd_country';
	const META_STATE_COUNTY = 'cmbd_stateCounty';
	const META_REGION = 'cmbd_region';
	const META_ICON = '_cmloc_icon';
	const META_LAT = '_cmloc_latitude';
	const META_LONG = '_cmloc_longitude';
	const META_GEOCODE_UP_TO_DATE = '_cmloc_geocode_up_to_date';
	
	
	/**
	 * Get instance
	 * 
	 * @param WP_Post|int $post Post object or ID
	 * @return com\cminds\maplocations\model\Business
	 */
	static function getInstance($post) {
		return parent::getInstance($post);
	}
	
	
	static function registerPostType() {
		
// 		self::createExampleRecords();
		
		// Don't register
		return;
		
	}
	
	
	
	static function createExampleRecords() {
		global $wpdb;
		
		$wpdb->delete($wpdb->postmeta, array('meta_key' => static::META_LAT));
		$wpdb->delete($wpdb->postmeta, array('meta_key' => static::META_LONG));
		$wpdb->delete($wpdb->postmeta, array('meta_key' => static::META_GEOCODE_UP_TO_DATE));
		
		
// 		die('aaaaaaaa');
		
		$streets = array('Wojska Polskiego', 'Warszawska', 'Wrocławska', 'Poznańska', 'Armii Krajowej', 'Jana Pawła II', 'Długa', 'Krótka', 'Kazimierza Wielkiego', 'Batorego');
		$cities = array('Zielona Góra', 'Gdańsk', 'Poznań', 'Wrocław', 'Warszawa', 'Kraków', 'Łódź', 'Białystok', 'Bydgoszcz', 'Szczecin');
		
		$i=0;
		foreach ($streets as $s => $street) {
			foreach ($cities as $c => $city) {
				$i++;
				$bus = new static(array(
					'post_parent' => '',
					'post_author' => get_current_user_id(),
					'post_type' => static::POST_TYPE,
					'post_status' => 'publish',
					'ping_status' => 'closed',
					'comment_status' => 'closed',
				));
				$bus->setTitle('Business '. rand());
				$bus->setContent(str_repeat($bus->getTitle() . ' is located at '. $street .' street in '. $city .' Poland. ', 10));
// 				var_dump($bus);
				$bus->save();
				$bus->setPostMeta(static::META_ADDRESS, $street);
				$bus->setPostMeta(static::META_CITY_TOWN, $city);
				$bus->setPostMeta(static::META_COUNTRY, 'Poland');
			}
		}
		var_dump($i);
		exit;
		
	}
	
	
	
	function getAddress() {
		return $this->getPostMeta(static::META_ADDRESS);
	}
	
	
	function getCityTown() {
		return $this->getPostMeta(static::META_CITY_TOWN);
	}
	
	function getPostalCode() {
		return $this->getPostMeta(static::META_POSTAL_CODE);
	}
	
	function getCountry() {
		return $this->getPostMeta(static::META_COUNTRY);
	}
	
	function getstateCounty() {
		return $this->getPostMeta(static::META_STATE_COUNTY);
	}
	
	function getRegion() {
		return $this->getPostMeta(static::META_REGION);
	}
	
	
	function getPostMetaKey($name) {
		return $name;
	}
	
	
	function getFullAddress() {
		return $this->getAddress() .' '. $this->getCityTown() .' '. $this->getPostalCode() .' '. $this->getCountry();
	}
	
	
	function getFullAddressFormatted() {
		return $this->getAddress() .'<br />'. $this->getPostalCode() .' '. $this->getCityTown() .'<br />'. $this->getCountry();
	}
	
	function getCategories($fields = TaxonomyTerm::FIELDS_MODEL, $params = array()) {
		return BusinessCategory::getPostTerms($this->getId(), $fields, $params);
	}
	
	
	function getIconUrl() {
		$icon = $this->getPostMeta(static::META_ICON);
		if (empty($icon)) {
			if ($categories = $this->getCategories() AND $category = reset($categories)) {
				/* @var $category BusinessCategory */
				$icon = $category->getIcon();
			}
		}
		if (empty($icon)) $icon = null;
		return $icon;
	}
	
	
	static function getIndexMapJSLocations($query = null) {
		global $wpdb;
		
		$records = apply_filters('cmloc_business_index_map_pre_results', array(), $query);
		
		if (empty($records)) {
			$records = BusinessLocationsQuery::getResults($query);
// 			$records = static::getIndexMapJSLocationsResults($query);
		}
		
		foreach ($records as $i => $row) {
			$records[$i]['type'] = Location::TYPE_LOCATION;
			/* @var $business Business */
			if ($business = Business::getInstance($row['id'])) {
				$records[$i]['permalink'] = $business->getPermalink();
				$records[$i]['icon'] = $business->getIconUrl();
// 				$records[$i]['infowindow'] = $business->getInfoWindow();
				Business::clearInstances();
			}
		}
		
		return $records;
	
	}
	
	
	function getInfoWindow() {
		return sprintf('<div class="cmloc-infowindow"><h2><a href="%s">%s</a></h2>%s</div>',
			esc_attr($this->getPermalink()),
			esc_html($this->getTitle()),
			$this->getFullAddressFormatted()
		);
	}
	
	
	function getLat() {
		return $this->getPostMeta(static::META_LAT);
	}
	
	function setLat($lat) {
		return $this->setPostMeta(static::META_LAT, $lat);
	}
	
	function getLong() {
		return $this->getPostMeta(static::META_LONG);
	}
	
	function setLong($long) {
		return $this->setPostMeta(static::META_LONG, $long);
	}
	
	
	function needGeocode() {
		return ($this->getPostMeta(static::META_GEOCODE_UP_TO_DATE) != 1);
	}
	
	
	function setGeocodeUpToDate($val) {
		return $this->setPostMeta(static::META_GEOCODE_UP_TO_DATE, intval($val));
	}
	
	
	function geocodeAddress() {
		
		$fullAddress = $this->getFullAddress();
		if (strlen(preg_replace('/\s/', '', str_replace($this->getCountry(), '', $fullAddress))) == 0) {
			return null;
		}
		
		$ctx = stream_context_create(array('http'=>
			array(
				'timeout' => 15,
			)
		));
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?address='. urlencode($this->getFullAddress()) .'&sensor=false';
		$response = file_get_contents($url, false, $ctx);
		$response = json_decode($response, true);
// 		var_dump($response);
		if ($response AND isset($response['status']) AND $response['status'] == 'OK' AND isset($response['results'][0])) {
			return $response;
		}
	}
	
	
	function geocodeLocationFromAddress() {
		$response = $this->geocodeAddress();
		if ($response AND isset($response['results'][0])) {
			return $response['results'][0]['geometry']['location'];
		}
	}
	
	
	function setLocationFromAddress() {
		if ($this->needGeocode()) {
			if ($location = $this->geocodeLocationFromAddress()) {
				$this->setLat($location['lat']);
				$this->setLong($location['lng']);
			} else {
				delete_post_meta($this->getId(), static::META_LAT);
				delete_post_meta($this->getId(), static::META_LONG);
			}
			$this->setGeocodeUpToDate(true);
		}
	}
	
	
	
	static function getIdsNeedGeolocation() {
		global $wpdb;
		return $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts p
			LEFT JOIN $wpdb->postmeta m ON p.ID = m.post_id AND m.meta_key = %s
			WHERE post_type = %s
				AND post_status = 'publish'
				AND (m.meta_id IS NULL OR m.meta_value = 0)
			LIMIT 100",
			static::META_GEOCODE_UP_TO_DATE,
			static::POST_TYPE
		));
	}
	
	
	static function cleanLocationForEmptyAddresses() {
		global $wpdb;
		$ids = $wpdb->get_col($wpdb->prepare("SELECT p.ID
			FROM $wpdb->posts p
			LEFT JOIN $wpdb->postmeta addr ON p.ID = addr.post_id AND addr.meta_key = %s
			LEFT JOIN $wpdb->postmeta citytown ON p.ID = citytown.post_id AND citytown.meta_key = %s
			LEFT JOIN $wpdb->postmeta postalcode ON p.ID = postalcode.post_id AND postalcode.meta_key = %s
			WHERE
				p.post_type = %s
				AND (addr.meta_value IS NULL OR addr.meta_value = '')
				AND (citytown.meta_value IS NULL OR citytown.meta_value = '')
				AND (postalcode.meta_value IS NULL OR postalcode.meta_value = '')
			",
			static::META_ADDRESS,
			static::META_CITY_TOWN,
			static::META_POSTAL_CODE,
			static::POST_TYPE
		));
		
		if (empty($ids)) return;
		
		$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta
			WHERE (meta_key = %s OR meta_key = %s)
			AND post_id IN (". implode(',', $ids) .")",
			static::META_LAT,
			static::META_LONG
		));
		
		foreach ($ids as $id) {
			update_post_meta($id, static::META_GEOCODE_UP_TO_DATE, 1);
		}
		
	}
	
	
}
