<?php

namespace com\cminds\maplocations\model;

class BusinessCategory extends TaxonomyTerm {

	const TAXONOMY = 'cm-business-category';
	const CATEGORY_PERMALINK_PART = 'cm-business-category';
	const OPTION_LOCATIONS_DEFAULT_ICONS = 'cmbd_category_icons';
	
	
	static function getUrlPart() {
		return self::CATEGORY_PERMALINK_PART;
	}
	
    
    /**
	 * Get instance
	 * 
	 * @param object|int $term Term object or ID
	 * @return com\cminds\maplocations\model\BusinessCategory
	 */
	static function getInstance($term) {
		return parent::getInstance($term);
	}
	
	
	function getIcon() {
		$options = get_option(self::OPTION_LOCATIONS_DEFAULT_ICONS, array());
		$id = $this->getId();
		if (isset($options[$id])) {
			return $options[$id];
		}
	}
	
	

	function setIcon($icon) {
		$options = get_option(self::OPTION_LOCATIONS_DEFAULT_ICONS, array());
		$id = $this->getId();
		$options[$id] = $icon;
		update_option(self::OPTION_LOCATIONS_DEFAULT_ICONS, $options);
		return $this;
	}
	
	
    
}
