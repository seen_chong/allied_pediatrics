=== Plugin Name ===
Name: CM Map Locations Pro
Contributors: CreativeMindsSolutions
Donate link: https://www.cminds.com/
Tags:location,locations,trail,maps,google,google maps,point of interest,address map,business locations,coordinates,custom google maps,dealer locator,geo,geocoder,geocoding,google map,google map plugin,google map widget,google shortcode,latitude,location finder,locator,locator maps,longitude, map,map creator,map directions,map maker,map markers,map plugin,map tools,map widget,mapper,mapping,mapping software,mapping tools,marker,places,shop finder,shop locator,shops,store finder,store locator,store locator map,stores,wordpress locator,wp google map,wp google maps,wp maps,zip code,zip code locator,zip code search,zipcode,zipcode locator,zipcode search,business address map,business locations,business maps
Requires at least: 4.0
Tested up to: 4.7.0
Stable tag: 1.3.3

Manage locations and support location finder using Google Maps.

== Description ==

Manage locations and support location finder using Google Maps.


> #### Plugin Site
> * [Plugin Site](https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/)
> * [Pro Version Detailed Features List](https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/)
> * Plugin demo [Read Only mode](https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/).

---
 
 
> #### Follow Us
> [Blog](http://plugin.cminds.com/blog/) | [Twitter](http://twitter.com/cmplugins)  | [Google+](https://plus.google.com/108513627228464018583/) | [LinkedIn](https://www.linkedin.com/company/creativeminds) | [YouTube](https://www.youtube.com/user/cmindschannel) | [Pinterest](http://www.pinterest.com/cmplugins/) | [FaceBook](https://www.facebook.com/cmplugins/)


**More Plugins by CreativeMinds**

* [CM Ad Changer](http://wordpress.org/plugins/cm-ad-changer/) - Manage, Track and Report Advertising Campaigns Across Sites. Can turn your Turn your WP into an Ad Server
* [CM Super ToolTip Glossary](http://wordpress.org/extend/plugins/enhanced-tooltipglossary/) - Easily creates a Glossary, Encyclopaedia or Dictionary of your website's terms and shows them as a tooltip in posts and pages when hovering. With many more powerful features.
* [CM Download Manager](http://wordpress.org/extend/plugins/cm-download-manager) - Allows users to upload, manage, track and support documents or files in a download directory listing database for others to contribute, use and comment upon.
* [CM MicroPayments](https://plugins.cminds.com/cm-micropayment-platform/) - Adds the in-site support for your own "virtual currency". The purpose of this plugin is to allow in-site transactions without the necessity of processing the external payments each time (quicker & easier). Developers can use it as a platform to integrate with their own plugins.
* [CM Video Tutorials](https://wordpress.org/plugins/cm-plugins-video-tutorials/) - Video Tutorials showing how to use WordPress and CM Plugins like Q&A Discussion Forum, Glossary, Download Manager, Ad Changer and more.
* [CM OnBoarding](https://wordpress.org/plugins/cm-onboarding/) - Superb Guidance tool which improves the online experience and the user satisfaction.


== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

> [More FAQ's](https://www.cminds.com/store/map-locations-plugin-for-wordpress-by-creativeminds/)
>



== Changelog ==
= 1.3.3 =
* Fixed PHP error.

= 1.3.2 =
* Fixed issue with importing CSV file.
* Added new column to the CSV file to import the categories list.

= 1.3.1 =
* Performance optimization for the index map.

= 1.3.0 =
* Added searching by ZIP code with a radius parameter on the index page.
* Added an option to use the web browser's geolocation API to search by the ZIP code.
* Added an option to change the default location marker icon.
* Fixed a JS error.

= 1.2.5 =
* Updated licensing api support.

= 1.2.4 =
* Fixed bug with inserting images to the location.

= 1.2.3 =
* Added option to show current user's location using the browser's Geolocation API.
* Added new parameters for the cmmrm-cmloc-common-map: path=1 and categoryfilter=1.
* Fixed CM Business Directory map shortcode.

= 1.2.2 =
* Fixed displaying CM Business Directory locations.

= 1.2.1 =
* Fixed issue with including page templates from child theme.
* Fixed issue with map not being displayed on the edit page.

= 1.2.0 =
* Added CSV import feature.

= 1.1.2 =
* Added option to show map on the index page for CM Business Directory Pro plugin.

= 1.1.1 =
* Added option to change the default map type: roadmap, satellite, terrain, hybrid.
* Added option to set the custom map icon URL for the CM Business Directory Pro plugin categories.
* Display adjustments.

= 1.1.0 =
* Fixed pagination issue.
* Updated licensing api support.

= 1.0.19 =
* Fixed settings issue.
* Fixed issue with adding image to location.
* Fixed PHP bug.
* Fixed is_home issue for some themes.

= 1.0.18 =
* Fixed PHP error.
* Fixed is_home issue.
* Fixed settings issue.

= 1.0.17 =
* Fixed issues related to new Wordpress version.

= 1.0.16 =
* Added new fields: phone number, website URL and email.

= 1.0.15 =
* Updated licensing api support.

= 1.0.14 =
* Fixed the licensing issue causing the AJAX didn't work.

= 1.0.13 =
* Added option to use rich text editor or simple editor for the location description.

= 1.0.12 =
* Added search by zip and state

= 1.0.11 =
* Fixed issue with Business map shortcode.
* Improved the search feature: searching by address and zip code.

= 1.0.10 =
* Fixed issue with locations query.

= 1.0.9 =
* CSS fix.

= 1.0.8 =
* Updated licensing support.

= 1.0.7 =
* Fixed PHP issue with WP Query.

= 1.0.4 =
* Integration with CM Business Directory.
* Added new shortcode cmloc-business.
* Added user uploaded icons per location.

= 1.0.3 =
* Added new parameters to the map shortcode. Made category param optional.
* Displaying paginated locations list in the map shortcode.
* Updated licensing api support.

= 1.0.2 =
* Added option to choose page template for the front-end.
* Added option to change the index page ordering.
* Added option to include location description in the tooltip.
* Added option to show tooltip when clicking on the list.

= 1.0.1 =
* Added admin notification to create a menu sidebar widget.

= 1.0.0 =
* Initial release
