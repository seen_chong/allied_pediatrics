<?php

namespace com\cminds\maplocations\shortcode;

use com\cminds\maplocations\controller\BusinessController;

use com\cminds\maplocations\model\BusinessCategory;

use com\cminds\maplocations\model\Business;

use com\cminds\maplocations\helper\RouteView;

use com\cminds\maplocations\model\MapRoute;

use com\cminds\maplocations\controller\FrontendController;

use com\cminds\maplocations\model\Settings;

use com\cminds\maplocations\controller\RouteController;

use com\cminds\maplocations\model\Route;

class BusinessShortcode extends Shortcode {
	
	const SHORTCODE_NAME = 'cmloc-business';
	
	
	static function shortcode($atts) {
		
		$atts = shortcode_atts(array(
			'category' => null,
			'categoryfilter' => 1,
			'searchbar' => 1,
			's' => '',
			'ids' => '',
		), $atts);
		
		
		if ($categoryObj = BusinessCategory::getInstance($atts['category'])) {
			$currentCategoryId = $categoryObj->getId();
			$atts['category'] = $categoryObj->getSlug();
		} else {
			$atts['category'] = $currentCategoryId = '';
		}
		
		$query = array(
			'post_type' => Business::POST_TYPE,
			'post_status' => 'publish',
			BusinessCategory::TAXONOMY => (!empty($atts['category']) ? $atts['category'] : null),
			'posts_per_page' => -1,
			'paged' => 1,
			's' => $atts['s'],
			'post__in' => array_filter(is_array($atts['ids']) ? $atts['ids'] : explode(',', $atts['ids'])),
			'ignore_sticky' => true,
		);
		
		$routes = Business::getIndexMapJSLocations($query);
		
// 		if ($routes) {
			
			FrontendController::enqueueStyle();
			wp_enqueue_script('cmloc-index-map');
			wp_enqueue_script('cmloc-business-map-shortcode');
			
			RouteController::loadSinglePageScripts();
			$displayParams = Settings::getOption(Settings::OPTION_INDEX_ROUTE_PARAMS);
			
			$content = RouteController::loadFrontendView('index-map', compact('routes', 'atts'));
			
			if ($atts['categoryfilter']) {
				$categories = BusinessCategory::getTreeArray();
				$content = BusinessController::loadFrontendView('filter', compact('categories', 'atts', 'currentCategoryId')) . $content;
			}
			
			return sprintf('<div class="cmloc-map-shortcode cmloc-locations-archive cmloc-business-shortcode"%s>%s</div>',
				RouteView::getDisplayParams($displayParams),
				$content
			);
// 		}
		
	}
	
	
}
