<?php

namespace com\cminds\maplocations\shortcode;

use com\cminds\maplocations\model\Category;

use com\cminds\maplocations\helper\RouteView;

use com\cminds\maplocations\model\MapRoute;

use com\cminds\maplocations\controller\FrontendController;

use com\cminds\maplocations\model\Settings;

use com\cminds\maplocations\controller\RouteController;

use com\cminds\maplocations\model\Route;

class MapRoutesCommonShortcode extends Shortcode {
	
	const SHORTCODE_NAME = 'cmmrm-cmloc-common-map';
	const CACHE_PREFIX = '_cmloccache_common_shcode_';
	const CACHE_TIME = 0;
	
	
	static function shortcode($atts) {
		
// 		$start = microtime(true);
		
		$atts = shortcode_atts(array(
			'path' => 0,
			'categoryfilter' => 0,
			'cache' => 0,
		), $atts);
		
		// Disabled the caching feature since the script has been moved to wp_footer
		$atts['cache'] = 0;
		
		// Load resources
		FrontendController::enqueueStyle();
		wp_enqueue_script('cmloc-index-map');
		RouteController::loadSinglePageScripts();
		if ($atts['categoryfilter']) {
			wp_enqueue_script('cmloc-common-map-filter');
		}
		
		// Check cache
		$cacheName = static::CACHE_PREFIX . md5(serialize($atts));
		if ($atts['cache']) {
			$data = get_option($cacheName);
			if (!empty($data)) {
				return $data;
			}
		}
		
		$result = '';
		
		// Query
		$routes = MapRoute::getIndexMapJSLocations();
// 		$stop = microtime(true);var_dump(round(($stop-$start)*1000));
		if ($routes) {
			
			$displayParams = Settings::getOption(Settings::OPTION_INDEX_ROUTE_PARAMS);
			if ($atts['path']) $displayParams[] = 'overview-path';
			
			$content = '';
			if ($atts['categoryfilter']) {
				$categories = Category::getTreeArray();
				$content .= RouteController::loadFrontendView('common-map-category-filter', compact('routes', 'atts', 'categories'));
			}
			$content .= RouteController::loadFrontendView('index-map', compact('routes', 'atts'));
			
			$result = sprintf('<div class="cmloc-map-shortcode cmloc-routes-common-shortcode cmloc-locations-archive"%s>%s</div>',
				RouteView::getDisplayParams($displayParams),
				$content
			);
			
		}
		
		if ($atts['cache']) {
			update_option($cacheName, $result, false);
		}
		
// 		$stop = microtime(true);var_dump(round(($stop-$start)*1000));
		
		return $result;
		
	}
	
	
}
