<?php

namespace com\cminds\maplocations\shortcode;

use com\cminds\maplocations\controller\FrontendController;

use com\cminds\maplocations\helper\RouteView;

use com\cminds\maplocations\model\Category;

use com\cminds\maplocations\model\Settings;

use com\cminds\maplocations\controller\RouteController;

use com\cminds\maplocations\model\Route;

class MapShortcode extends Shortcode {
	
	const SHORTCODE_NAME = 'cmloc-locations-map';
	
	
	static function shortcode($atts) {
		
		$atts = shortcode_atts(array(
			'category' => null,
			'limit' => 5,
			'page' => 1,
			'list' => 'none',
		), $atts);
		
		$query = new \WP_Query(array(
			'post_type' => Route::POST_TYPE,
			'post_status' => 'publish',
			Category::TAXONOMY => (!empty($atts['category']) ? $atts['category'] : null),
			'posts_per_page' => $atts['limit'],
			'paged' => $atts['page'],
		));
		$routes = Route::getIndexMapJSLocations($query);
		
		if (!empty($routes)) {
			
			FrontendController::enqueueStyle();
			wp_enqueue_script('cmloc-index-map');
			wp_enqueue_script('cmloc-map-shortcode');
			
			RouteController::loadSinglePageScripts();
			$displayParams = Settings::getOption(Settings::OPTION_INDEX_ROUTE_PARAMS);
			
			$content = RouteController::loadFrontendView('index-map', compact('routes'));
			if ($atts['list'] != 'none') $content .= RouteController::getIndexList($query);
			
			$shortcodeId = 'cmloc-map-shortcode-' . mt_rand();
			$content .= sprintf('<script type="text/javascript">jQuery(function($) { CMLOC_Map_Shortcode(%s); });</script>',
				json_encode($shortcodeId)
			);
			
			return sprintf('<div class="cmloc-map-shortcode cmloc-locations-archive cmloc-layout-%s" id="%s" data-use-ajax="1" data-shortcode-params="%s"%s>%s</div>',
				esc_attr($atts['list']),
				esc_attr($shortcodeId),
				esc_attr(json_encode($atts)),
				RouteView::getDisplayParams($displayParams),
				$content
			);
			
		}
		
	}
	
	
}
