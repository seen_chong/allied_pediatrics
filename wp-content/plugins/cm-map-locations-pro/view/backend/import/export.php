<?php


use com\cminds\maplocations\controller\DashboardController;

?>

<div class="cmloc-block">
	<form action="<?php echo esc_attr($formUrl); ?>" method="post" enctype="multipart/form-data" id="cmloc-export-form">
	<h3>Export all locations</h3>
	<p>Download a ZIP archive with KML files for each location.</p>
	<p><input type="hidden" name="<?php echo $nonceField; ?>" value="<?php echo $nonce; ?>" /><input type="submit" value="Export KML files" /></p>
	</form>
</div>
