<?php


use com\cminds\maplocations\App;

use com\cminds\maplocations\controller\ImportController;

?>

<div class="cmloc-block">

	<form action="<?php echo esc_attr($formUrl); ?>" method="post" enctype="multipart/form-data" id="cmloc-import-route-form" target="cmloc-import-csv-frame">
		<h3>Import multiple locations from CSV</h3>
		<p>Select CSV file to import: <input type="file" name="csv_file" /></p>
		<p><input type="hidden" name="<?php echo $nonceField; ?>" value="<?php echo $nonce; ?>" /><input type="submit" value="Import CSV" /></p>
	</form>
	
	<h4>CSV format:</h4>
	<ul>
		<li><strong>location name</strong></li>
		<li><strong>latitude</strong></li>
		<li><strong>longitude</strong></li>
		<li><strong>description</strong></li>
		<li><strong>status</strong> - <kbd>publish</kbd> or <kbd>draft</kbd></li>
		<li><strong>address</strong></li>
		<li><strong>postal code</strong></li>
		<li><strong>tags</strong> - names of tags separated by comma</li>
		<li><strong>categories</strong> - names of categories separated by comma</li>
	</ul>
	<h4>Example:</h4>
	<pre class="cmloc-csv-example"><?php echo nl2br(file_get_contents(App::path('asset/import-example.csv'))); ?></pre>
	
	<iframe id="cmloc-import-csv-frame" name="cmloc-import-csv-frame"></iframe>

</div>
