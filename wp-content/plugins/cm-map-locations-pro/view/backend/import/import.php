<?php


use com\cminds\maplocations\controller\ImportController;

?>

<div class="cmloc-block">

	<form action="<?php echo esc_attr($formUrl); ?>" method="post" enctype="multipart/form-data" id="cmloc-import-route-form" target="cmloc-import-frame">
	<h3>Import location (KML, GPX)</h3>
	<?php echo ImportController::loadFrontendView('import-form-fields'); ?>
	<p><input type="hidden" name="<?php echo $nonceField; ?>" value="<?php echo $nonce; ?>" /><input type="submit" value="Import" /></p>
	</form>
	
	<iframe id="cmloc-import-frame" name="cmloc-import-frame"></iframe>

</div>
