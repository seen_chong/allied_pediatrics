<?php

use com\cminds\maplocations\model\Category;

use com\cminds\maplocations\helper\FormHtml;

use com\cminds\maplocations\model\Labels;

?>
<div class="cmloc-field cmloc-field-categories">
	<label><?php echo Labels::getLocalized('location_category'); ?>:</label>
	<?php echo FormHtml::checkboxTree('categories[]', $route->getCategories(Category::FIELDS_IDS), $categoriesTree); ?>
</div>