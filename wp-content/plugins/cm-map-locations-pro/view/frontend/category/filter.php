<?php 


use com\cminds\maplocations\model\Labels;

use com\cminds\maplocations\helper\RouteView;

?><div class="cmloc-categories-filter">
	<select class="cmloc-location-filter-category">
		<option value="<?php echo esc_attr($baseUrl); ?>">-- <?php echo Labels::getLocalized('filter_all_categories_opt'); ?> --</option>
		<?php echo RouteView::categoriesFilter($currentCategoryId, $categories); ?>
	</select>
</div>