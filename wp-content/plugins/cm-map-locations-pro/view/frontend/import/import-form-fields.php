<?php

use com\cminds\maplocations\model\Route;

use com\cminds\maplocations\model\Labels;

?>

<div class="cmloc-field">
	<span><?php echo Labels::getLocalized('dashboard_import_form_desc'); ?></span>
	<label><input type="file" name="cmloc_import_file" /></label>
</div>
