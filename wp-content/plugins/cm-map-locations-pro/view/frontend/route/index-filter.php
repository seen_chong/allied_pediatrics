<?php

use com\cminds\maplocations\model\Labels;


?><div class="cmloc-location-index-filter cmloc-filter">

	<form action="<?php echo esc_attr($searchFormUrl); ?>" class="cmloc-location-filter-form">
	
		<?php do_action('cmloc_map_filter_before'); ?>
	
		<label class="cmloc-field-search">
			<input type="text" name="s" value="<?php echo esc_attr(isset($_GET['s']) ? $_GET['s'] : '');
				?>" placeholder="<?php echo Labels::getLocalized('search_placeholder'); ?>" class="cmloc-input-search" autocomplete="off"/>
			<ul class="search-result-container"></ul>
		</label>
		
		<button type="submit" title="<?php echo esc_attr(Labels::getLocalized('search_btn')); ?>">
			<span>Search</span>
			<!-- <span class="dashicons dashicons-search"></span> -->
		</button>
		
		<?php do_action('cmloc_map_filter_after'); ?>
	
	</form>
	
</div>

<script>
	$(function(){
		var loading = false;
		$('.cmloc-field-search input').on('keyup', function(){
			var resultElement = $('.search-result-container');
			resultElement.empty();

			var value = $(this).val();
			var category = $('.cmloc-location-filter-category').val();
			if(value.length < 2){
				return;
			}

			loading = true;

			var link = category + '?s=' + value;
			var result = [];



			resultElement.append($('<li/>').text('Loading...'));

			$.get(link, function(html){
				resultElement.empty();
				loading = false;
				var $data = $('<div/>').html(html);

				$data.find('.cmloc-location-snippet').each(function(){
					var a = $(this).find('h2').find('a');
					result.push({
						url:a.attr('href'),
						text: a.text()
					});
				});

				for(var i = 0; i <= result.length - 1; i++){
					resultElement.append($('<li/>').append($('<a/>').attr('href', result[i]['url']).text(result[i]['text'])));
				}
			})
		});

		if($('.cmloc-field-search input').val().length >= 2){
			$('.cmloc-field-search input').trigger('keyup');
		}
	})
</script>