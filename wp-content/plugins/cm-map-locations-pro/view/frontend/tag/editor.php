<?php

use com\cminds\maplocations\model\TaxonomyTerm;

use com\cminds\maplocations\model\Labels;

?>
<div class="cmloc-field">
	<label><?php echo Labels::getLocalized('location_tags'); ?>:</label>
	<input type="text" name="tags" value="<?php echo esc_attr(implode(', ', $route->getTags(TaxonomyTerm::FIELDS_NAMES))); ?>" />
</div>