<?php

use com\cminds\maplocations\model\Labels;

?><div class="cmloc-widget-related-routes">
	<?php echo $args['before_title']; ?><?php echo Labels::getLocalized('widget_title_related_locations'); ?><?php echo $args['after_title']; ?>
	<ul><?php foreach ($routes as $route):
		printf('<li><a href="%s">%s</a></li>', esc_attr($route->getPermalink()), esc_html($route->getTitle()));
	endforeach; ?></ul>
</div>