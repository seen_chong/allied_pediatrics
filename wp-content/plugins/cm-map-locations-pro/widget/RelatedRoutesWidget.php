<?php

namespace com\cminds\maplocations\widget;

use com\cminds\maplocations\model\Settings;

use com\cminds\maplocations\controller\Controller;
use com\cminds\maplocations\controller\FrontendController;

use com\cminds\maplocations\App;
use com\cminds\maplocations\model\SettingsAbstract;

class RelatedRoutesWidget extends Widget {
	
	const WIDGET_NAME = 'CM Map Locations Related Locations';
	const WIDGET_DESCRIPTION = 'Displays related locations on the CM CM Map Locations single location page.';
	
	static protected $widgetFields = array(
		'limit' => array(
			'type' => Settings::TYPE_INT,
			'default' => 5,
			'label' => 'Limit',
		),
	);
	
	
	function getWidgetContent($args, $instance) {
		
		$instance = shortcode_atts(array(
			'limit' => static::$widgetFields['limit']['default'],
		), $instance);
		
		if (FrontendController::$query->is_single() AND $route = FrontendController::getRoute()) {
			$routes = $route->getRelatedRoutes($instance['limit']);
			if (!empty($routes)) {
				return Controller::loadView('frontend/widget/related-routes', compact('args', 'instance', 'routes'));
			}
		}
	}
	
	
	function canDisplay($args, $instance) {
		return FrontendController::isThePage();
	}
	

}
