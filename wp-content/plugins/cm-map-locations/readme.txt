=== Plugin Name ===
Name: CM Map Locations
Contributors: CreativeMindsSolutions
Donate link: https://www.cminds.com/
Tags:location,locations,trail,maps,google,google maps,point of interest,address map,business locations,coordinates,custom google maps,dealer locator,geo,geocoder,geocoding,google map,google map plugin,google map widget,google shortcode,latitude,location finder,locator,locator maps,longitude, map,map creator,map directions,map maker,map markers,map plugin,map tools,map widget,mapper,mapping,mapping software,mapping tools,marker,places,shop finder,shop locator,shops,store finder,store locator,store locator map,stores,wordpress locator,wp google map,wp google maps,wp maps,zip code,zip code locator,zip code search,zipcode,zipcode locator,zipcode search,business address map,business locations,business maps
Requires at least: 4.0
Tested up to: 4.6.0
Stable tag: 1.3.3

The Map Locations plugin lets users manage locations and support location finder using Google Maps integration.

== Description ==

The Google Map Locations plugins allows users to place a location, multiple locations, or point of interests on a Google map together with relevant information for this location such as description, images, video, full address, zip code routes and searchable markers. 

The Google Maps Location plugin shows all locations are then displayed on a Google map through Google Maps integration. Each location can have it's unique icon based on the category it belongs to or a specific set for each location using Google Maps navigation. 

The interactive plugin shows all locations in a directory using a mapping system which can support several use cases including a store locator system and using multiple maps.

[youtube https://www.youtube.com/watch?v=GLf4YK0-Hek]


Plugin supports importing and exporting locations, searching locations by zip code or any other relevant information, and embedding a location within any post or page using shortcodes.


> #### Plugin Site and Demo
> * [Plugin Site](https://www.cminds.com/wordpress-plugins-library/map-locations-plugin-for-wordpress-by-creativeminds/)
> * [Pro Version Detailed Features List](https://www.cminds.com/wordpress-plugins-library/map-locations-plugin-for-wordpress-by-creativeminds/#features)
> * [Plugin demo](http://www.knowledgetrail.com/map-locations/).
> * [Plugin User Guide](https://www.cminds.com/wordpress-plugins-knowledge-base-and-documentation/?hscat=568-cm-map-locations-cmml).

> #### Related Plugins
> * [CM Route Manager Plugin](https://wordpress.org/plugins/cm-route-manager/)
> * [CM Business Directory](https://wordpress.org/plugins/cm-business-directory/)

---
 
 **Basic Location Plugin Features**

* Place a location on a Google map
* Add description and images for each location
* Show all locations on an index page showing one map for all locations

**Benefits of Going Pro**

* locations index map display – Choose between several display templates to support use cases such as store locator, store list, point of interest and more.
* Import and Export Locations – Import and export locations using an XML format based on the KML scheme.
* Categories – Place locations in categories and assign a unique icon for each category.
* Weather information – Weather information is shown near each location.
* Location Icon – Override location category icon with a unique icon per each specific location.
* Upload Location Icon – Upload your own location icon.
* Images – Images and videos can be added to each location
* Shortcodes – Several shortcodes are supported. Shortcodes can be embedded in posts and show a single location, a map with all locations in a category, and more.
* Tags – Tags can be added to locations and allow filtering of locations
* Search – search by any keywords in location description or title.
* Search by zip / state – search dirwctly using zip or city or state
* Route Manager – Integrates with the Route Manager plugin to show both locations and routes on a joint map.
* Business Directory – Integrates with the Business Directory plugin to show all business as locations on map.

[youtube https://www.youtube.com/watch?v=Z7R-T3I_h3s]

---
> #### Other Plugins By CM Plugins
> * [Plugin Catalog](https://www.cminds.com/wordpress-plugins-library)
> * [Free Plugins](https://profiles.wordpress.org/creativemindssolutions#content-plugins)

---
 
> #### Follow Us
> [Blog](https://www.cminds.com/category/wordpress/) | [Twitter](http://twitter.com/cmplugins)  | [Google+](https://plus.google.com/u/0/+CmindsPlugins/) | [LinkedIn](https://www.linkedin.com/company/creativeminds) | [YouTube](https://www.youtube.com/user/cmindschannel) | [Pinterest](http://www.pinterest.com/cmplugins/) | [FaceBook](https://www.facebook.com/cmplugins/)

---

**Suggested Plugins by CreativeMinds**

* [CM Ad Changer](http://wordpress.org/plugins/cm-ad-changer/) - Manage, Track and Report Advertising Campaigns Across Sites. Can turn your Turn your WP into an Ad Server
* [CM Super ToolTip Glossary](http://wordpress.org/extend/plugins/enhanced-tooltipglossary/) - Easily creates a Glossary, Encyclopaedia or Dictionary of your website's terms and shows them as a tooltip in posts and pages when hovering. With many more powerful features.
* [CM Download Manager](http://wordpress.org/extend/plugins/cm-download-manager) - Allows users to upload, manage, track and support documents or files in a download directory listing database for others to contribute, use and comment upon.
* [CM MicroPayments](https://www.cminds.com/wordpress-plugins-library/micropayments/) - Adds the in-site support for your own "virtual currency". The purpose of this plugin is to allow in-site transactions without the necessity of processing the external payments each time (quicker & easier). Developers can use it as a platform to integrate with their own plugins.
* [CM Video Tutorials](https://wordpress.org/plugins/cm-plugins-video-tutorials/) - Video Tutorials showing how to use WordPress and CM Plugins like Q&A Discussion Forum, Glossary, Download Manager, Ad Changer and more.
* [CM OnBoarding](https://wordpress.org/plugins/cm-onboarding/) - Superb Guidance tool which improves the online experience and the user satisfaction.


== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add a Google API key.
4. Add as many locations as you want. Each will have it's own location page and will show up on the index map
5. There are a handful of other optional preferences available in the dashboard.


== Frequently Asked Questions ==

> [Plugin FAQ in CM Locations Plugin Site](https://www.cminds.com/wordpress-plugins-library/map-locations-plugin-for-wordpress-by-creativeminds/#plugin-faq)
>

== Screenshots ==

1. Showing location finder on Google map
2. Single location page.
3. Plugin setting options.
4. Locations admin dashboard.
5. Location editing - adding image and video.
6. Location editing metadata.
7. Location editing - updating address and zip.
8. Selecting icon to display on map.

== Changelog ==
> [View Release Notes in CM Locations Plugin Site](https://www.cminds.com/wordpress-plugins-library/map-locations-plugin-for-wordpress-by-creativeminds/#changelog)
>