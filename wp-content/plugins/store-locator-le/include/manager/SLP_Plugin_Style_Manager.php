<?php

if ( ! class_exists( 'SLP_Plugin_Style_Manager' ) ):

	/**
	 * class SLP_Plugin_Style_Manager
	 *
	 * @package   StoreLocatorPlus\PluginStyle\Manager
	 * @author    Lance Cleveland <lance@charlestonsw.com>
	 * @copyright 2012-2016 Charleston Software Associates, LLC     *
	 *
	 * @property    SLP_Plugin_Style $Plugin_Style
	 */
	class SLP_Plugin_Style_Manager extends SLP_Object_With_Objects {
		private   $plugin_style;
		protected $objects = array(
			'Plugin_Style' => array(
				'subdir' => 'include/unit/',
				'object' => null,
			),
		);

		/**
		 * Add the theme settings to the admin panel.
		 *
		 * @param   SLP_Settings $settings
		 */
		public function add_settings( $settings ) {
			$group_params = array( 'plugin' => $this->slplus, 'section_slug' => 'view', 'group_slug' => 'appearance' );

			$this->instantiate( 'Plugin_Style' );
			$theme_list = $this->Plugin_Style->get_theme_list();

			$the_description = __( 'Select a plugin style to change the CSS styling and layout of the slplus shortcode elements. ', 'store-locator-le' ) .
			                   __( 'This determines how the locator looks within your page. ', 'store-locator-le' ) .
			                   '<span class="new_paragraph">' .
			                   __( 'Most plugin styles work best with the following add ons installed: ', 'store-locator-le' ) .
			                   '<br/>' .
			                   $this->slplus->text_manager->get_web_link( 'shop_for_experience' ) .
			                   '<br/>' .
			                   $this->slplus->text_manager->get_web_link( 'shop_for_premier' ) .
			                   '</span>' .
			                   $this->slplus->text_manager->get_web_link( 'docs_for_plugin_style' ) .
			                   sprintf( '<p class="plugin_style_meta"><span class="label">%s %s</span></p>', __( 'Last Updated', 'store-locator-le' ), $this->slplus->options_nojs['themes_last_updated'] );


			$settings->add_ItemToGroup( array(
				'group_params' => $group_params,
				'label'        => __( 'Plugin Style', 'store-locator-le' ),
				'option_name'  => 'options_nojs',
				'option'       => 'theme',
				'type'         => 'list',
				'custom'       => $theme_list,
			) );

			$settings->add_ItemToGroup( array(
				'group_params' => $group_params,
				'type'         => 'subheader',
				'description'  => $the_description,
			) );

			// Add Style Details Divs
			//
			$settings->add_ItemToGroup( array(
				'group_params' => $group_params,
				'label'        => '',
				'description'  => $this->Plugin_Style->setup_ThemeDetails( $theme_list ),
				'setting'      => 'themedesc',
				'type'         => 'subheader',
			) );
		}

	}

endif;