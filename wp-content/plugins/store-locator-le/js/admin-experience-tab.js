/**
 * @package StoreLocatorPlus\Admin\ExperienceTab
 * @author Lance Cleveland <lance@storelocatorplus.com>
 * @copyright 2016 Charleston Software Associates, LLC
 *
 */

/**
 * Setting Helpers
 */
var slp_setting_helper = function () {
    var turned_on = new  Object();

    /*
     * Initialize our helpers.
     */
    this.initialize = function () {
        jQuery('div[data-related_to]').hover(SLP_ADMIN.helper.related_on, SLP_ADMIN.helper.related_off);
    }

    /**
     * Add 'highlight-related' CSS to all elements containing the related_to string in their name property.
     * Add those elements to the "turned on" list.
     */
    this.related_on = function () {
        var related_to = jQuery(this).attr('data-related_to');
        var related_array = related_to.split(',');
        related_array.forEach( item => {

            var related_element = jQuery('div[name*="' + item + '"]');
            jQuery(related_element).addClass('highlight-related');

            if ( typeof turned_on[item] === 'undefined' ) {
                turned_on[item] = related_element;
            }
        } );
    }

    /**
     * Remove 'highlight-related' CSS from all elements on the "turned on" list.
     */
    this.related_off = function () {
        for (var property in turned_on ) {
            if ( turned_on.hasOwnProperty(property) ) {
                var related_element = turned_on[property];
                jQuery( related_element ).removeClass('highlight-related');
                turned_on[property] = undefined;
            }
        }
    }

};


// Is our page loaded?  Go do stuff.
//
jQuery( document ).ready(
    function () {
        SLP_ADMIN.helper = new slp_setting_helper();
        SLP_ADMIN.helper.initialize();

    }
);