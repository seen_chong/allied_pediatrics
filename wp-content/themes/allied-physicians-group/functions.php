<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Genesis Sample Theme' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.0.1' );

//* Add HTML5 markup structure
add_theme_support( 'html5' );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

 //* Remove Footer
 remove_action('genesis_footer', 'genesis_do_footer');
 remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
 remove_action('genesis_footer', 'genesis_footer_markup_close', 15);

//* Move secondary nav menu 
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_before_content_sidebar_wrap', 'genesis_do_subnav' );

//* Reposition the breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
add_action( 'genesis_after_header', 'genesis_do_breadcrumbs' );

//* Add new featured image sizes */
add_image_size( 'featured-category', 100, 75, TRUE );

add_action( 'wp_enqueue_scripts', 'script_managment', 99);

/* ***********************************************************
********************** Register Widgets **********************
*********************************************************** */
 
//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'home-after-loop-blog-puddles',
	'name'        => __( 'Home - After Loop - Blog Puddles', '$text_domain' ),
	'description' => __( 'This is the section of the home page that displays blog excerpts.', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations */
add_action( 'genesis_before_footer', 'category_featured_posts' );
 
function category_featured_posts() {
 if (is_front_page() ) {
	if ( is_active_sidebar( 'home-after-loop-blog-puddles' ) ) {
	
		genesis_widget_area( 'home-after-loop-blog-puddles', array(
		'before' => '<div class="home-after-loop-blog-puddles widget-area">',
		'after'  => '</div>',
	) );
}}}

//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'custom-footer',
	'name'        => __( 'Custom Footer', '$text_domain' ),
	'description' => __( 'This is the custom footer section of the website', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations 
add_action( 'genesis_footer', 'custom_footer' );
 
function custom_footer() {
	if ( is_active_sidebar( 'custom-footer' ) ) {
	
		genesis_widget_area( 'custom-footer', array(
		'before' => '<div class="custom-footer widget-area">',
		'after'  => '</div>',
	) );
}}

//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'home-before-content',
	'name'        => __( 'Home - Before Content', '$text_domain' ),
	'description' => __( 'This is the before content section on the home page of the website', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations 
add_action( 'genesis_after_header', 'home_before_content' );
 
function home_before_content() {
	if (is_front_page() ) {
	if ( is_active_sidebar( 'home-before-content' ) ) {
	
		genesis_widget_area( 'home-before-content', array(
		'before' => '<div class="home-before-content widget-area">',
		'after'  => '</div>',
	) );
}}}

//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'menu-symptom-checker',
	'name'        => __( 'Symptom Checker Menu', '$text_domain' ),
	'description' => __( 'Acara Widget', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations */
add_action( 'genesis_entry_content', 'menu_symptom_checker', 0 );
 
function menu_symptom_checker() {
	$pages = array(807, 833, 888, 894, 896, 901, 904, 907, 910, 912, 921, 925, 929, 931, 933, 939, 942, 951, 954, 957, 956, 961, 963, 965, 974, 976, 979, 981, 983, 987, 989, 991, 995, 1006, 1008, 1010, 1012, 1014, 1017, 1019, 1021, 1024, 1026, 1038, 1040, 1043, 1046, 1050, 1053, 1055, 1058, 1063, 1066, 1069, 1071, 1073, 1075, 1077, 1079, 1082, 1087, 1110, 1116, 1131, 1134, 1143, 1150, 1153, 1161, 1164, 1166, 1171, 1178, 1182, 1186, 1189, 1191, 1193, 1195, 1197, 1199, 1203, 1206, 1209, 1213, 1215, 1217, 1219, 1221, 1230, 1180, 1169, 1158, 1154, 1137, 1130, 1128, 1124, 1122, 1118, 1113, 1086);
    if ( is_page( $pages ) ) {
	if ( is_active_sidebar( 'menu-symptom-checker' ) ) {
	
		genesis_widget_area( 'menu-symptom-checker', array(
		'before' => '<div class="menu-symptom-checker">',
		'after'  => '</div>',
	) );
}}}

//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'disclaimer-symptom-checker',
	'name'        => __( 'Symptom Checker Disclaimer', '$text_domain' ),
	'description' => __( 'Acara Widget', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations */
add_action( 'genesis_entry_content', 'disclaimer_symptom_checker', 10 );
 
function disclaimer_symptom_checker() {
	$pages = array(833, 888, 894, 896, 901, 904, 907, 910, 912, 921, 925, 929, 931, 933, 939, 942, 951, 954, 957, 956, 961, 963, 965, 974, 976, 979, 981, 983, 987, 989, 991, 995, 1006, 1008, 1010, 1012, 1014, 1017, 1019, 1021, 1024, 1026, 1038, 1040, 1043, 1046, 1050, 1053, 1055, 1058, 1063, 1066, 1069, 1071, 1073, 1075, 1077, 1079, 1082, 1087, 1110, 1116, 1131, 1134, 1143, 1150, 1153, 1161, 1164, 1166, 1171, 1178, 1182, 1186, 1189, 1191, 1193, 1195, 1197, 1199, 1203, 1206, 1209, 1213, 1215, 1217, 1219, 1221, 1230, 1180, 1169, 1158, 1154, 1137, 1130, 1128, 1124, 1122, 1118, 1113, 1086);
    if ( is_page( $pages ) ) {
	if ( is_active_sidebar( 'disclaimer-symptom-checker' ) ) {
	
		genesis_widget_area( 'disclaimer-symptom-checker', array(
		'before' => '<div class="disclaimer-symptom-checker">',
		'after'  => '</div>',
	) );
}}}


//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'menu-dosage-checker',
	'name'        => __( 'Dosage Checker Menu', '$text_domain' ),
	'description' => __( 'Acara Widget', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations */
add_action( 'genesis_entry_content', 'menu_dosage_checker', 0 );
 
function menu_dosage_checker() {
	$pages = array(1600, 1615, 1621, 1628);
    if ( is_page( $pages ) ) {
	if ( is_active_sidebar( 'menu-dosage-checker' ) ) {
	
		genesis_widget_area( 'menu-dosage-checker', array(
		'before' => '<div class="menu-dosage-checker">',
		'after'  => '</div>',
	) );
}}}

//* Register New Widget 
genesis_register_sidebar( array(
	'id'          => 'disclaimer-dosage-checker',
	'name'        => __( 'Dosage Checker Disclaimer', '$text_domain' ),
	'description' => __( 'Acara Widget', '$text_domain' ),
) ); 
//** Change The Conditional Tags & Hook To Display In Different Positions & Locations */
add_action( 'genesis_entry_content', 'disclaimer_dosage_checker', 10 );
 
function disclaimer_dosage_checker() {
	$pages = array(1600, 1615, 1621);
    if ( is_page( $pages ) ) {
	if ( is_active_sidebar( 'disclaimer-dosage-checker' ) ) {
	
		genesis_widget_area( 'disclaimer-dosage-checker', array(
		'before' => '<div class="disclaimer-dosage-checker">',
		'after'  => '</div>',
	) );
}}}


/* **********************************************************************
********************** Genesis Framework Functions **********************
********************************************************************** */

//* Remove page title for multiple pages 
add_action( 'get_header', 'child_remove_page_titles' );
function child_remove_page_titles() {
    $pages = array(5);
    if ( is_page( $pages ) ) {
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
    }
}

//* Remove breadcrumbs from Home page
add_action( 'get_header', 'remove_breadcrumbs_on_specific_pages' );
function remove_breadcrumbs_on_specific_pages() {
    $pages = array(5);
    if ( is_page( $pages ) ) {
        remove_action( 'genesis_after_header', 'genesis_do_breadcrumbs' );
    }
}

//* Add breadcrumbs to sub pages
add_filter( 'genesis_breadcrumb_args', 'sp_breadcrumb_args' );
function sp_breadcrumb_args( $args ) {
	$args['home'] = 'Home';
	$args['prefix'] = '<div class="breadcrumb wrap link-fade">';
	$args['labels']['prefix'] = '';
return $args;
}

//* Display a custom favicon
add_filter( 'genesis_pre_load_favicon', 'sp_favicon_filter' );
function sp_favicon_filter( $favicon_url ) {
	return 'http://pediatricmed.alliedphysiciansgroup.com/wp-content/uploads/2015/01/allied-physicians-group-favicon.png';
}

/* **************************************************************************
********************** Enqueue Scripts and Stylesheets **********************
************************************************************************** */

//* Use Google CDN jQuery and jQuery-UI Library
function script_managment() {
	  wp_deregister_script( 'jquery' );
	  wp_deregister_script( 'jquery-ui' );
	  wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js' );
	  wp_register_script( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js' );
	  wp_enqueue_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js', array( 'jquery' ), '4.0', false );
	  wp_enqueue_script( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js', array( 'jquery' ), '1.8.16' );
}


//* Enqueue Bootstrap 2 Grid stylesheet
add_action('wp_enqueue_scripts', 'enqueue_bootstrap2grid_stylesheet');
function enqueue_bootstrap2grid_stylesheet() {
    wp_register_style('bootstrap2grid_stylesheet', get_stylesheet_directory_uri().'/css/bootstrap2-grid.css', array());
    wp_enqueue_style('bootstrap2grid_stylesheet');
}

//* Enqueue Font Awesome Stylesheet from MaxCDN
add_action( 'wp_enqueue_scripts', 'webendev_load_font_awesome', 99 );
function webendev_load_font_awesome() {
	if ( ! is_admin() ) {
		wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css', null, '4.0.1' );
	}
}

//* Enqueue Fonts.com Helvetica Neue
add_action( 'wp_enqueue_scripts', 'enqueue_helvetica_neue' );
function enqueue_helvetica_neue() {
	wp_enqueue_style( 'fonts-helvetica-neue', '//fast.fonts.net/cssapi/3b2a0f09-2016-4bbb-89d0-93056fd2e6cc.css', array(), CHILD_THEME_VERSION );
}

//* Enqueue responsive menu script
add_action( 'wp_enqueue_scripts', 'enqueue_responsive_menu_script' );
function enqueue_responsive_menu_script() {
 wp_enqueue_script( 'responsive_menu_script', get_stylesheet_directory_uri() . '/js/responsive-menu/responsive-menu.js', array( 'jquery' ), '1.0.0', true ); 
}

//* Magnific Popup
add_action('wp_enqueue_scripts', 'enqueue_magnificpopup_styles');
function enqueue_magnificpopup_styles() {
    wp_register_style('magnific_popup_style', get_stylesheet_directory_uri().'/js/magnific-popup/magnific-popup.css', array());
    wp_enqueue_style('magnific_popup_style');
}
 
add_action('wp_enqueue_scripts', 'enqueue_magnificpopup_scripts');
function enqueue_magnificpopup_scripts() {
    wp_register_script('magnific_popup_script', get_stylesheet_directory_uri().'/js/magnific-popup/jquery.magnific-popup.min.js', array('jquery'));
    wp_enqueue_script('magnific_popup_script');
    wp_register_script('magnific_init_script', get_stylesheet_directory_uri().'/js/magnific-popup/jquery.magnific-popup-init.js', array('jquery'));
    wp_enqueue_script('magnific_init_script');
}

//* Enqueue master slider main css
add_action('wp_enqueue_scripts', 'enqueue_masterslider_main_css');
function enqueue_masterslider_main_css() {
    wp_register_style('masterslider_main_css', get_stylesheet_directory_uri().'/js/masterslider/style/masterslider.css', array());
    wp_enqueue_style('masterslider_main_css');
}
 
//* Enqueue master slider default skin css
add_action('wp_enqueue_scripts', 'enqueue_masterslider_default_skin_css');
function enqueue_masterslider_default_skin_css() {
    wp_register_style('masterslider_default_skin_css', get_stylesheet_directory_uri().'/js/masterslider/skins/default/style.css', array());
    wp_enqueue_style('masterslider_default_skin_css');
}

//* Enqueue master slider main js
add_action( 'wp_enqueue_scripts', 'enqueue_masterslider_main_js' );
function enqueue_masterslider_main_js() {
 wp_enqueue_script( 'masterslider_main_js', get_stylesheet_directory_uri() . '/js/masterslider/masterslider.js', array( 'jquery' ), '1.0.0', true ); 
}

//* Enqueue master slider main js
add_action( 'wp_enqueue_scripts', 'enqueue_masterslider_init_js' );
function enqueue_masterslider_init_js() {
 wp_enqueue_script( 'masterslider_init_js', get_stylesheet_directory_uri() . '/js/masterslider/masterslider.init.js', array( 'jquery' ), '1.0.0', true ); 
}

//* Enqueue google maps js
add_action( 'wp_enqueue_scripts', 'enqueue_google_maps_js' );
function enqueue_google_maps_js() {
wp_enqueue_script( 'google_maps_js', get_stylesheet_directory_uri() . '/js/google-maps/google-maps.js', array( 'jquery' ), '1.0.0', true ); 
}

//* Enqueue zurb responsive table js
add_action( 'wp_enqueue_scripts', 'enqueue_zurb_responsive_table_js' );
function enqueue_zurb_responsive_table_js() {
wp_enqueue_script( 'zurb_responsive_table_js', get_stylesheet_directory_uri() . '/js/zurb-responsive-tables/responsive-tables.js', array( 'jquery' ), '1.0.0', true ); 
}

//* Enqueue zurb responsive table css
add_action('wp_enqueue_scripts', 'enqueue_zurb_responsive_table_css');
function enqueue_zurb_responsive_table_css() {
    wp_register_style('zurb_responsive_table_css', get_stylesheet_directory_uri() .'/js/zurb-responsive-tables/responsive-tables.css', array());
    wp_enqueue_style('zurb_responsive_table_css');
}

add_filter( 'genesis_seo_title', 'child_header_title', 10, 3 );

function child_header_title( $title, $inside, $wrap ) {
   $inside = sprintf( '<a href="http://alliedphysiciansgroup.com/" title="%s">%s</a>', esc_attr( get_bloginfo( 'name' ) ), get_bloginfo( 'name' ) );
   return sprintf( '<%1$s class="site-title">%2$s</%1$s>', $wrap, $inside );
}