var slider = new MasterSlider();
	    slider.setup('masterslider' , {
	         width:1300,    // slider standard width
	         height:400,   // slider standard height
	         space:1,
	         view:"fade",
	         mouse:false,
	         autoplay:true,
	         loop:true,
	         fullwidth:true,

	         // more slider options goes here...
	         // check slider options section in documentation for more options.
	    });
	    // adds Arrows navigation control to the slider.
	    slider.control('arrows');